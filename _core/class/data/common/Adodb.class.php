<?php

/**
 * Crea una instancia de ADODB, libreria de abstracción de Base Datos
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */

class Adodb
{
	private static $singleton;

	/**
	 * @var ADODB_mysql $db
	 */
	private $db;

	/**
	 * Constructor
	 */
	private function __construct()
	{
		require(Config::create()->getCorePlugins().'Adodb/adodb-exceptions.inc.php');
		require(Config::create()->getCorePlugins().'Adodb/adodb.inc.php');

		Config::create()->setDataBase();

		/**
		 * @var ADODB_mysql $db
		 */
		$db = NewADOConnection(Config::create()->getDatabase()->driver);

		$db->host		= Config::create()->getDatabase()->host;

		$db->database	= Config::create()->getDatabase()->dataBase;

		$db->user		= Config::create()->getDatabase()->user;

		$db->password	= Config::create()->getDatabase()->pass;

		$this->db = $db;

		// Configuración
		$this->adodbConfig();

		// Eliminación de cache
		if ( isset($_GET['cache']) && $_GET['cache']=='flush' )
			$this->db->cacheSecs = 0;

		// Debug de querys
		if ( isset($_GET['debug']) && ($_GET['debug']=='z2akv3exc7rqlsjq8pic' || (strpos($_SERVER['HTTP_HOST'], 'dev.')!==false && $_GET['debug']==true)) )
			$this->db->debug = 1;

		$this->AdodbConnect();

	}

	/**
	 * @return Adodb
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * Configuración adicional de ADODB
	 */
	private function adodbConfig()
	{
		global $ADODB_LANG, $ADODB_FORCE_TYPE, $ADODB_CACHE_DIR;

		// Memcached
		$this->db->memCache = false;
		$this->db->memCacheHost = array('172.17.200.242'); /// $db->memCacheHost = $ip1; will work too
		$this->db->memCachePort = 11211; /// this is default memCache port
		$this->db->memCacheCompress = false;

		// Tiempo de caché de los recordset
		$this->db->cacheSecs = 60 * Config::create()->getAdodbCacheTime();

		$this->db->clientFlags = 65536;  // MYSQL_CLIENT_COMPRESS

		$this->db->SetFetchMode(2);

		$ADODB_LANG = 'es';

		$ADODB_FORCE_TYPE = ADODB_FORCE_NULL;

		$ADODB_CACHE_DIR = Config::create()->getServerCache().'adodb_cache';
	}

	/**
	 * Realiza la conexión con la BBDD
	 * @return ADODB_mysql
	 */
	public function AdodbConnect()
	{
		$level = error_reporting(E_ALL);

		try {
			$this->db->Connect();

			$this->db->Execute("SET NAMES 'utf8'");

			error_reporting($level);
		}
		catch ( Exception $error ) {

			error_reporting($level);

			FLogger::create()->error($error);
		}

		return $this->db;
	}

	/**
	 * Ejecuta una sentencia SQL
	 * @param string|bool $sql:			String Sql
	 * @param array|bool $params:		Array de parámetros
	 * @param int|null $secs2cache:		Tiempo de caché
	 * @return ADORecordSet|false
	 */
	public function Execute($sql=false, $params=false, $secs2cache=null)
	{
		$res = false;

		$level = error_reporting(E_ALL);

		try {
			if ( is_null($secs2cache) || $secs2cache=='' )
				$secs2cache = 0;

			if ( !$secs2cache )
				$res = $this->db->Execute($sql, $params);
			else
				$res = $this->db->CacheExecute($secs2cache, $sql, $params);

			error_reporting($level);
		}
		catch( Exception $error ) {

			error_reporting($level);

			FLogger::create()->error($error);
		}

		return $res;
	}

	/**
	 * Ejecuta una sentencia SQL desde el Caché
	 * @param string|bool $sql:			String Sql
	 * @param array|bool $params:		Array de parámetros
	 * @param int|null $secs2cache:		Tiempo de caché
	 * @return ADORecordSet|false
	 */
	public function CacheExecute($sql=false, $params=false, $secs2cache=null)
	{
		if ( is_null($secs2cache) || $secs2cache=='' )
			$secs2cache = $this->db->cacheSecs;

		return $this->Execute($sql, $params, $secs2cache);
	}

	/**
	 * Obtiene un registro
	 * @param string $sql:				String Sql
	 * @param array|bool $params:		Array de parámetros
	 * @return array|bool
	 */
	public function GetRow($sql, $params=array())
	{
		return $this->db->GetRow($sql, $params);
	}

	/**
	 * Destructor
	 */
	public function __destruct()
	{
		unset($this->resulset);

		$this->db->Close();
	}
}

?>