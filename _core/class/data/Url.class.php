<?php

require_once (Config::create()->getCoreModel().'Url.class.php');
require_once (Config::create()->getCoreModel().'Template.class.php');
require_once (Config::create()->getCoreModel().'Clase.class.php');

/**
 * Chequea que sea una url válida y agrega cantidad de visitas
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class UrlDao {

	/**
	 * @var ADODB_mysql
	 */
	private $db;

	/**
	 * @var UrlVO
	 */
	private $vo;

	/**
	 *Constructor
	 */
	public function __construct()
	{
		$this->db = Adodb::create();
	}

	/**
	 * Chequea que sea una url válida
	 * @param String $url
	 * @return bool
	 */
	public function isValid($url)
	{
		$response = false;

		if ( strpos($url, '.')===false )
			preg_match('/([a-z0-9\-]+\/)$/', $url, $match);

		$cacheTime = 0; //60 * 60; // en caché por 1 hora;

		$res = $this->db->CacheExecute('CALL sys_getUrl(?)', array(rawurldecode($url)), $cacheTime);

		if ( $res && $res->RecordCount() ) {

			$row = $res->FetchNextObj();

			$res->Close();

			$this->setVO($row);

			$response = true;
		}

		return $response;
	}

	/**
	 * Suma +1 a la cantidad de visitas de una url, ya sea carpeta o articulo
	 * @param int $id:		Id del artículo
	 * @param int $idc:		Id de la carpeta
	 * @return bool
	 */
	public function addVisita($id, $idc)
	{
		$response = false;

		if ( $id && $idc ) {

			$id = (int) $id;
			$idc = (int) $idc;

			$this->db->Execute('CALL sys_setVisitas(?, ?)', array($id, $idc));

			$response = true;
		}

		return $response;
	}

	/**
	 * @param $row
	 */
	private function setVO($row)
	{
		$vo = new UrlVO();

		$vo->setId($row->id);

		$vo->setTipo($row->tipo);

		if ( isset($row->idCarpeta) )
			$vo->setIdCarpeta($row->idCarpeta);

		if ( isset($row->idPadre) )
			$vo->setIdPadre($row->idPadre);

		if ( isset($row->carpeta) )
			$vo->setCarpeta($row->carpeta);
		
		if ( isset($row->parentsUrl) )
			$vo->setParentUrl($row->parentsUrl);

		$claseVO = new ClaseVO();
		$templateVO = new TemplateVO();

		if ( isset($row->idClase) )
			$claseVO->setId($row->idClase);

		if ( isset($row->clase) )
			$claseVO->setNombre($row->clase);

		$vo->setClaseVO($claseVO);

		if ( isset($row->idTemplate) )
			$templateVO->setId($row->idTemplate);

		if ( isset($row->template) )
			$templateVO->setNombre($row->template);

		$vo->setTemplateVO($templateVO);

		$this->vo = $vo;
	}

	/**
	 * Retorna un objeto UrlVo
	 * @return UrlVo
	 */
	public function getVO()
	{
		return $this->vo;
	}
}

?>