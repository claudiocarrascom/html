<?php

require_once (Config::create()->getCoreModel().'Carpeta.class.php');
require_once (Config::create()->getCoreModel().'Template.class.php');
require_once (Config::create()->getCoreModel().'Clase.class.php');

/**
 * Class DAO de Carpetas
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class CarpetaDao {

	/**
	 * @var ADODB_mysql
	 */
	private $db;

	/**
	 * @var CarpetaVO
	 */
	private $vo;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->db = Adodb::create();
	}

	/**
	 * Obtiene un registro de Carpeta
	 * @param int $id:	Id de la Carpeta
	 * @return bool
	 */
	public function getCarpeta($id)
	{
		$res = $this->db->CacheExecute('CALL sys_getCarpeta(?)', array($id));

		if ( $res && $res->RecordCount() ) {

			$this->setVO($res->FetchNextObj());

			$res->Close();
		}

		return ( $res ) ? true : false;
	}

	/**
	 * Obtiene un arreglo de Carpeta
	 * @param int $id:	Id de la Carpeta
	 * @return CarpetaVO[]
	 */
	public function getCarpetas($id)
	{
		$array = array();

		$sql = 'CALL sys_getCarpetas(?)';
		$res = $this->db->CacheExecute($sql, array($id));
		if ( $res ) {
			while ( !$res->EOF ) {
				$this->setVO($res->FetchNextObj());
				$array[] = $this->getVO();
			}
			$res->Close();
		}

		return $array;
	}

	/**
	 * Obtiene un arreglo de Carpeta para Menú
	 * @return CarpetaVO[]
	 */
	public function getCarpetasMenu()
	{
		$array = array();

		$sql = 'CALL sys_getCarpetasMenu()';
		$res = $this->db->CacheExecute($sql);
		if ( $res ) {
			while ( !$res->EOF ) {
				$this->setVO($res->FetchNextObj());
				$array[] = $this->getVO();
			}
			$res->Close();
		}

		return $array;
	}

	/**
	 * @param $row
	 */
	private function setVO($row)
	{
		$vo = new CarpetaVO();

		$claseVO = new ClaseVO();

		if ( isset($row->idClass) )
			$claseVO->setId($row->idClass);

		if ( isset($row->clase) )
			$claseVO->setNombre($row->clase);

		$templateVO = new TemplateVO();

		if ( isset($row->idTemplate) )
			$templateVO->setId($row->idTemplate);

		if ( isset($row->template) )
			$templateVO->setNombre($row->template);

		if ( isset($row->id) )
			$vo->setId($row->id);

		if ( isset($row->idPadre) )
			$vo->setIdPadre($row->idPadre);

		if ( isset($row->carpeta) )
			$vo->setCarpeta($row->carpeta);

		if ( isset($row->url) )
			$vo->setUrl($row->url);

		if ( isset($row->orden) )
			$vo->setOrden($row->orden);

		if ( isset($row->parentsUrl) )
			$vo->setParentUrl($row->parentsUrl);

		if ( isset($row->titulo) )
			$vo->setTitulo($row->titulo);

		if ( isset($row->descripcion) )
			$vo->setDescripcion($row->descripcion);

		if ( isset($row->keywords) )
			$vo->setKeywords($row->keywords);

		if ( isset($row->config) )
			$vo->setConfig($row->config);

		if ( isset($row->estado) )
			$vo->setEstado($row->estado);

		$vo->setTemplateVO($templateVO);

		$vo->setClaseVO($claseVO);

		$this->vo = $vo;
	}

	/**
	 * Retorna un objeto CarpetaVo
	 * @return CarpetaVo
	 */
	public function getVO()
	{
		return $this->vo;
	}
}

?>
