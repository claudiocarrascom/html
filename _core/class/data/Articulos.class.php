<?php

require_once (Config::create()->getCoreModel().'Articulos.class.php');
require_once (Config::create()->getCoreModel().'Template.class.php');
require_once (Config::create()->getCoreModel().'Clase.class.php');

/**
 * Artículos DAO
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */

class ArticulosDao {

	/**
	 * @var Adodb_mysql
	 */
	private $db;

	/**
	 * @var int
	 */
	private $internalCache = 1800;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->db = Adodb::create();
	}

	/**
	 * Obtiene un registro de Artículo
	 * @param int $id:			Id del artículo
	 * @param int $idcarpeta:	Id de la carpeta
	 * @return bool|ArticuloVO
	 */
	public function getArticulo($id)
	{
		$vo = false;

		$id = (int) $id;

		$res = $this->db->CacheExecute('CALL sys_getArticulo(?)', array($id));

		if ( $res ) {
			$vo = $this->setVO($res->FetchNextObj());
			$res->Close();
		}
		return $vo;
	}

	/**
	 * Obtiene articulos no destacados
	 * @param int $carpeta:		Identificador de la carpeta
	 * @param int $page:		Número de página
	 * @param int $limit:		Cantidad de registros a retornar
	 * @param array $excludes:	Artículos excluidos de los resultados
	 * @return bool|ArticuloVO[]
	 */
	public function getArticulos($carpeta, $page, $limit, $excludes)
	{
		$array['data'] = array();
		$array['total'] = 0;

		$res = $this->db()->Execute('CALL sys_getArticulos(?,?,?,?)', array($carpeta, $page, $limit, implode(',', $excludes)));

		if ( $res ) {
			while ( !$res->EOF ) {
				$array['data'][] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();

			$sql = 'SELECT FOUND_ROWS() AS total';
			$row = $this->db->GetRow($sql);
			if ( $row ) {
				$array['total'] = $row['total'];
				$array['pages'] = ceil($array['total'] / $limit);
			};
		}
		return $array;
	}

	/**
	 * Obtener artículos mas visitados
	 * @param int $interval:	Intervalo de tiempo en días
	 * @param int $limit:		Cantidad de registros requeridos
	 * @return ArticuloVO[]
	 */
	public function getMasVisitados($interval, $limit)
	{
		$array = array();
		$res = $this->db->CacheExecute('CALL sys_getArticulosMasVistos(?, ?)', array($interval, $limit), $this->internalCache);
		if ( is_object($res) ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();
		}
		return $array;
	}

	/**
	 * Obtener artículos mas visitados de una carpeta específica
	 * @param int $interval:	Intervalo de tiempo en días
	 * @param int $limit:		Cantidad de registros requeridos
	 * @param int $carpeta:		Carpeta específica
	 * @return ArticuloVO[]
	 */
	public function getMasVisitadosCarpeta($interval, $limit, $carpeta)
	{
		$array = array();
		$res = $this->db->CacheExecute('CALL sys_getArticulosMasVistosCarpeta(?, ?, ?)', array($interval, $limit, $carpeta), $this->internalCache);
		if ( is_object($res) ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();
		}
		return $array;
	}

	/**
	 * Obtener últimos artículos publicados
	 * @param int $limit:		Cantidad de registros requeridos
	 * @return ArticuloVO[]
	 */
	public function getUltimosArticulos($limit)
	{
		$array = array();

		$limit = (int) $limit;

		$res = $this->db->CacheExecute('CALL sys_getUltimoMinuto(?)', array($limit));
		if ( is_object($res) ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();
		}
		return $array;
	}

	/**
	 * Obtiene por id carpeta padre, el último artículo públicados de cada carpeta hijo
	 * @param int $carpeta:	Identificador de la carpeta
	 * @param int $limit:		Cantidad de registros requeridos
	 * @return ArticuloVO[]
	 */
	public function getUltimosArticulosCarpeta($carpeta, $limit)
	{
		$array = array();

		$res = $this->db->CacheExecute('CALL sys_getArticulosUltimoCarpeta(?, ?)', array($carpeta, $limit));
		if ( is_object($res) ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();
		}
		return $array;
	}

	/**
	 * Método abstracto para oarticulos adyacentes
	 * @param int $id:			Identificador de artículo
	 * @param int $carpeta:		Identificador de la carpeta
	 * @param int $curso:		Identificador de dirrecion, PRE O POST
	 * @param int $limit:		Limite de resultados
	 * @return bool|ArticuloVO[]
	 */
	public function getArticulosAdyacentes($id, $carpeta, $curso, $limit)
	{
		$array = array();
		$res = $this->db()->CacheExecute('CALL sys_getArticulosAdyacentes(?,?,?,?)', array($id, $carpeta, $curso, $limit));
		if ( is_object($res) ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();
		}
		return $array;

	}

	/**
	 * Método abstracto para obtener artículos destacados
	 * @param int $carpeta:		Identificador de la carpeta
	 * @param int $limite:		Limite de resultados
	 * @return bool|ArticuloVO[]
	 */
	public function getDestacados($carpeta, $limite)
	{
		$array = false;

		$res = $this->db()->CacheExecute('CALL sys_getArticulosDestacados(?,?)', array($carpeta, $limite));
		if ( $res ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO( $res->FetchNextObj() );
			}
			$res->Close();
		}
		return $array;

	}
	/**
	 * Obtiene articulos relacionados
	 * @param int $id		:	Id de articulo
	 * @param string $rel	:	Tipo de Relacionado (A=Artículo|G=Galeria|V=Video)
	 * @return ArticuloVO[]
	 */
	public function getRelacionados($id,$rel)
	{
		$array = array();

		$res = $this->db->CacheExecute('CALL sys_getArticulosRelacionados(?,?)',array($id,$rel));
		if ( is_object($res) ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();
		}
		return $array;
	}

	/**
	 * Obtiene articulos según un parámetro de búsqueda
	 * @param string $search:	Texto de búsqueda
	 * @param int $carpeta:		Id de Carpeta o NULL para busqueda sobre el sitio
	 * @param int $page:		Número de página
	 * @param int $limit:		Cantidad de registros a retornar
	 * @param string $tipo:		Tipo de búsqueda
	 * @return array
	 */
	public function getBusqueda($search, $carpeta, $page, $limit, $tipo)
	{
		$array['data'] = array();
		$array['total'] = 0;

		$search = preg_replace('/\s{2,}/', ' ', $search);

		if ( $tipo=='search' ){
			$explode = array();
			foreach ( explode(' ', $search) as $value )
				$explode[] = '+'.$value;

			$search = implode(' ', $explode);
		}

		$res = $this->db->Execute('CALL sys_getBuscador(?, ?, ?, ?, ?)', array($search, $carpeta, $page, $limit, $tipo));
		if ( is_object($res) ) {
			while ( !$res->EOF ) {
				$array['data'][] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();

			$sql = 'SELECT FOUND_ROWS() AS total';
			$row = $this->db->GetRow($sql);
			if ( $row ) {
				$array['total'] = $row['total'];
				$array['pages'] = ceil($array['total'] / $limit);
			};
		}
		return $array;
	}

	/**
	 * Obtiene articulos para Galerías
	 * @return ArticuloVO[]
	 */
	public function getGalerias($limit)
	{
		$array = array();

		$res = $this->db->CacheExecute('CALL sys_getGalerias(?)', array($limit));
		if ( is_object($res) ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();
		}
		return $array;
	}

	/**
	 * Obtiene articulos de una Carpeta específica para Fotogalería
	 * @return ArticuloVO[]
	 */
	public function getGaleriaCarpeta($limit, $idCarpeta)
	{
		$array = array();

		$res = $this->db->CacheExecute('CALL sys_getGaleriaCarpeta(?,?)', array($limit, $idCarpeta));
		if ( is_object($res) ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();
		}
		return $array;
	}

	/**
	 * Set el VO del artículo
	 * @param Object $row
	 * @return ArticuloVO
	 */
	public function setVO($row)
	{
		$vo = new ArticuloVO();

		if ( isset($row->id) )
			$vo->set('id', $row->id);

		if ( isset($row->idCarpeta) )
			$vo->set('idCarpeta', $row->idCarpeta);

		if ( isset($row->idRootParent) )
			$vo->set('idRootCarpeta', $row->idRootParent);

		if ( isset($row->carpeta) )
			$vo->set('carpeta', $row->carpeta);

		if ( isset($row->idCarpeta) )
			$vo->set('idCarpetaArt', $row->idCarpeta);

		if ( isset($row->titulo) )
			$vo->set('titulo', $row->titulo);

		if ( isset($row->tituloAlternativo) )
			$vo->set('tituloAlternativo', $row->tituloAlternativo);

		if ( isset($row->bajada) )
			$vo->set('bajada', $row->bajada);

		if ( isset($row->texto) )
			$vo->set('texto', $row->texto);

		if ( isset($row->tags) )
			$vo->set('tags', $row->tags);

		if ( isset($row->fechaCreacion) )
			$vo->set('fechaCreacion', $row->fechaCreacion);

		if ( isset($row->fechaPublicacion) )
			$vo->set('fechaPublicacion', $row->fechaPublicacion);

		if ( isset($row->url) )
			$vo->set('url', $row->url);

		if ( isset($row->urlCorta) )
			$vo->set('urlCorta', $row->urlCorta);

		if ( isset($row->periodista) )
			$vo->set('periodista', $row->periodista);

		if ( isset($row->imagenDescripcion) )
			$vo->set('imagenDescripcion', $row->imagenDescripcion);

		if ( isset($row->urlCarpeta) )
			$vo->set('urlCarpeta', $row->urlCarpeta);

		if ( isset($row->urlAlternativa) )
			$vo->set('urlAlternativa', $row->urlAlternativa);

		if ( isset($row->imagenNombre) )
			$vo->set('imgNombre', $row->imagenNombre);

		if ( isset($row->imagenUrl) )
			$vo->set('imgUrl', Config::create()->getHttpImages().$row->imagenUrl);

		if ( isset($row->imagenUrl) )
			$vo->set('imgUrlSrv', Config::create()->getServerImages().$row->imagenUrl);

		if ( isset($row->videoNombre) )
			$vo->set('videoNombre', $row->videoNombre);

		if ( isset($row->visitas) )
			$vo->set('visitas', $row->visitas);

		if ( isset($row->posicionDestacado) )
			$vo->set('posicion', $row->posicionDestacado);

		if ( isset($row->vigencia) )
			$vo->set('vigencia', $row->vigencia);

		if ( isset($row->estado) )
			$vo->set('estado', $row->estado);

		if ( isset($row->seccion) )
			$vo->set('seccion', $row->seccion);

		if ( isset($row->imagenes) )
			$vo->set('imagenes', $row->imagenes);

		$claseVO = new ClaseVO();
		if ( isset($row->idClass) )
			$claseVO->setId($row->idClass);

		if ( isset($row->clase) )
			$claseVO->setNombre($row->clase);

		$templateVO = new TemplateVO();

		if ( isset($row->idTemplate) )
			$templateVO->setId($row->idTemplate);

		if ( isset($row->template) )
			$templateVO->setNombre($row->template);

		$vo->setTemplateVO($templateVO);

		$vo->setClaseVO($claseVO);

		return $vo;
	}

	/**
	 * Retorna la referencia a ADODB
	 * @return Adodb|Adodb_mysql
	 */
	public function db()
	{
		return $this->db;
	}
}

?>