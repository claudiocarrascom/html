<?php

require_once (Config::create()->getCoreModel().'Multimedia.class.php');

/**
 * Multimedia DAO
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class MultimediaDao {

	/**
	 * @var ADODB_mysql
	 */
	private $db;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->db = Adodb::create();
	}

	/**
	 * Retorna todos loa archivos asociados a un artículo según tipo requerido
	 * @param int $ida:		Id del Artículo
	 * @param int $tipo:	Tipo de Multimedia solicitada
	 * @return MultimediaVO[]
	 */
	public function getMultimedia($ida, $tipo)
	{
		$array = array();

		$sql = 'CALL sys_getMultimedias(?, ?)';
		$res = $this->db->CacheExecute($sql, array($ida, $tipo));
		if ( $res ) {
			while ( !$res->EOF ) {
				$array[] = $this->setVO($res->FetchNextObj());
			}
			$res->Close();
		}

		return $array;
	}

	/**
	 * Set el VO de Multimedia
	 * @param object $row
	 * @return MultimediaVO
	 */
	private function setVO($row)
	{
		$vo = new MultimediaVO();

		if ( isset($row->id) )
			$vo->set('id', $row->id);

		if ( isset($row->tipo) )
			$vo->set('tipo', $row->tipo);

		if ( isset($row->nombre) )
			$vo->set('nombre', $row->nombre);

		if ( isset($row->original) )
			$vo->set('nombreOriginal', $row->original);

		if ( isset($row->descripcion) )
			$vo->set('descripcion', $row->descripcion);

		if ( isset($row->videoTipo) )
			$vo->set('videoTipo', $row->videoTipo);

		if ( isset($row->crc) )
			$vo->set('crc', $row->crc);

		if ( isset($row->orden) )
			$vo->set('orden', $row->orden);

		if ( isset($row->estado) )
			$vo->set('estado', $row->estado);

		if ( isset($row->url) ) {

			// Tipo 1:Video
			if ( $vo->getTipo()==1 ) {

				$video = Config::create()->getHttpVideos();

				if ( $vo->getNombre() )
					$vo->set('url', NULL);
                    //$vo->set('nombre', NULL);

				elseif ( $vo->getNombreOriginal() ) {
                    $vo->set('url', $video[2].$row->url);
                    $vo->set('nombre', $vo->getNombreOriginal());

				}
			}

			// Tipo 2:Imágenes
			elseif ( $vo->getTipo()==2 )
				$vo->set('url', Config::create()->getHttpImages().$row->url);

			// Tipo 3:Documentos
			elseif ( $vo->getTipo()==3 )
				$vo->set('url', Config::create()->getHttpImages().$row->url);
		}

		return $vo;
	}
}

?>