<?php

if ( is_file(Config::create()->getWebsiteBussiness().'common/Transversal.class.php') )
	require_once(Config::create()->getWebsiteBussiness().'common/Transversal.class.php');

/**
 * Módulo con funciones comunes para las clases del Sitio
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class Module {

	private $id;

	private $class;

	/**
	 * @var TemplateVO
	 */
	private $template;

	/**
	 * @var CarpetaVO|ArticuloVo
	 */
	private $vo;

	/**
	 * @var Smarty
	 */
	private $smarty;

	/**
	 * @var bool
	 */
	private $movil;

	/**
	 * @var string
	 */
	private $device;

	/**
	 * @var array
	 */
	private $smartyValues;

	/**
	 * Constructor
	 * @param UrlVo|CarpetaVO|ArticuloVO $vo
	 */
	public function __construct($vo)
	{
		$this->setClass();
		
		$this->setId($vo->getId());

		$this->setVo($vo);
		
		$this->setTemplate($vo->getTemplateVO());

		$this->setSmarty();

		$this->setMovile();

		$this->setDevice();
	}

	/**
	 * Nombre de la Class actual
	 */
	public function setClass()
	{
		$this->class = get_class($this);
	}

	/**
	 * Retorna el nombre de la clase
	 */
	public function getClass()
	{
		return strtolower($this->class);
	}

	/**
	 * Define el ID de la Carpeta
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Retorna el ID de la Carpeta
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Define título de la página
	 * @param string $title
	 */
	public function setTitle($title)
	{
		if ( empty($title) )
			$title = Config::create()->getSiteTitle();

		SeoTags::create()->setTitle($title);
	}

	/**
	 * Retorna el Título de la Página
	 * @return int
	 */
	public function getTitle()
	{
		return SeoTags::create()->getTitle();
	}

	/**
	 * Define la Descripción de la página
	 * @param string $description
	 */
	public function setDescription($description)
	{
		if ( empty($description) )
			$description = Config::create()->getSiteDescription();

		SeoTags::create()->setDescription($description);
	}

	/**
	 * Retorna la Descripción de la página
	 * @return int
	 */
	public function getDescription()
	{
		return SeoTags::create()->getDescription();
	}

	/**
	 * Define las Keywords de la página
	 * @param string $keywords
	 */
	public function setKeywords($keywords)
	{
		if ( empty($keywords) )
			$keywords = Config::create()->getSiteKeywords();

		SeoTags::create()->setKeywords($keywords);
	}

	/**
	 * Retorna las Keywords de la página
	 * @return int
	 */
	public function getKeywords()
	{
		return SeoTags::create()->getKeywords();
	}

	/**
	 * Define los datos del Template
	 * @param CarpetaVO|ArticuloVO $vo
	 */
	public function setVo($vo)
	{
		$this->vo = $vo;
	}

	/**
	 * Retorna un objeto de tipo CarpetaVO
	 * @return CarpetaVO|ArticuloVo
	 */
	public function getVo()
	{
		return $this->vo;
	}

	/**
	 * Define los datos del Template
	 * @param TemplateVO $template
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
	}

	/**
	 * Retorna un objeto de tipo TemplateVO
	 * @return TemplateVO
	 */
	public function getTemplate()
	{
		return $this->template;
	}

	/**
	 * Configuraciones de Smarty
	 */
	private function setSmarty()
	{
		$this->smarty = new Smarty();
		//$this->smarty->caching = true;
		//$this->smarty->cache_lifetime = 60 * Config::create()->getSmartyCacheTime();

		// Liberación del caché
		if ( isset($_GET['cache']) && $_GET['cache']=='flush' )
			$this->smarty->cache_lifetime = 0;

		$this->smarty->setTemplateDir( Config::create()->getSmartyTemplate() );
		$this->smarty->setCompileDir( Config::create()->getSmartyCompile() );
		$this->smarty->setConfigDir( Config::create()->getSmartyConfig() );
		$this->smarty->setCacheDir( Config::create()->getSmartyCache() );
	}

	/**
	 * Retorna la instancia de smarty
	 * @return Smarty
	 */
	public function getSmarty()
	{
		return $this->smarty;
	}

	/**
	 * Define valores a enviar al template
	 * @param string $key:		Nombre clave del valor a guardar
	 * @param string $value:	Valor a guardar
	 */
	public function setValue($key, $value)
	{
		$this->smartyValues[$key] = $value;
	}

	/**
	 * Detección del tipo de dispositivo
	 */
	private function setMovile()
	{
		$movil = new Mobile_Detect();
		$this->movil = (bool) $movil->isMobile();
	}

	/**
	 * Detección del tipo de dispositivo
	 * @return bool
	 */
	private function getMovile()
	{
		return $this->movil;
	}

	/**
	 * Detección del tipo de dispositivo (tablet, phone, computer)
	 */
	private function setDevice()
	{
		$detect = new Mobile_Detect();

		$this->device = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
	}

	/**
	 * Detección del tipo de dispositivo (tablet, phone, computer)
	 * @return string
	 */
	public function getDevice()
	{
		return $this->device;
	}

	/**
	 * @return string
	 */
	private function getOasPage()
	{
		$url = Request::create()->getUrlPath();
		
		$excludes = array('programas', 'teleseries');
		
		foreach ( $excludes as $exclude ) {
			
			$exclude .= '/';
			
			if (!($url == $exclude || $url == $exclude)) {
					
				if ( strpos($url, $exclude) !== false )
					$url = substr($url, strlen($exclude));				
			}
		}

		if ( substr($url, -1)=='/' )
			$url = substr($url, 0, -1);

		//$url = str_replace('.html', '', $url);

		return $url;
	}

	/**
	 * Despliega los datos a pantalla
	 * @var string $dirname:	Carpeta por defecto del template
	 */
	public function display($dirname='')
	{
		if ( $dirname )
			$dirname .= '/';

		// Tags Google SEO
		$site['title']				= Config::create()->getSiteTitle(); //$this->getTitle();
		$site['description']		= Config::create()->getSiteDescription(); //$this->getDescription();
		$site['keywords']			= Config::create()->getSiteKeywords(); //$this->getKeywords();

		$site['expires']			= date('D, d M Y H:i:s ', strtotime('+7 days')).'GMT';
		$site['webRoot']			= Config::create()->getHttpWebsite();
		$site['sectionRoot']		= Config::create()->getHttpWebsite() . $dirname . $this->getClass() . '/';
		$site['directory']			= Config::create()->getWebsiteTemplates();
		$site['urlHome'] 			= Config::create()->getHttpHome();
		$site['urlBase']			= Config::create()->getHttpBase();
		//$site['urlSite']			= Config::create()->getHttpBase() . $this->getVo()->getParentUrl();

		if ( method_exists(Config::create(), 'getHttpStatic') )
			$site['urlStatic']		= Config::create()->getHttpStatic();

		if ( method_exists(Config::create(), 'getHttpResources') )
			$site['urlResources']	= Config::create()->getHttpResources();

		if ( class_exists('Transversal') ) {
			if ( method_exists(Transversal::create(), 'getOasPage') )
				$site['oasPage']	= Transversal::create()->getOasPage();
		}

		if ( !isset($site['oasPage']) )
			$site['oasPage']		= $this->getOasPage();

		$site['isSmartphone']		= $this->getDevice()=='phone' ? true: false;
		$site['isTablet']			= $this->getDevice()=='tablet' ? true : false;
		$site['isComputer']			= $this->getDevice()=='computer' ? true : false;
		
		$site['isMovil']			= $this->getMovile();

		$site['analytics']['id']	= Config::create()->getAnalyticsId();
		$site['analytics']['site']	= Config::create()->getAnalyticsSite();

		$site['comscore']['id']		= ComScore::create()->getId();
		$site['comscore']['site']	= ComScore::create()->getSite();
		$site['comscore']['name']	= ComScore::create()->getName();
		//$site['comscore']['c3']		= Config::create()->getComScoreSite();
		$site['comscore']['c3']		= ComScore::create()->getC3();

		$this->smarty->assign('site', $site);

		if ( isset($this->smartyValues['javascript']) )
			$this->smarty->assign('javascript', $this->smartyValues['javascript']);

		$this->smarty->assign('data', $this->smartyValues);

		// Head adicional
		$site['head'] = '';
		if ( is_file( Config::create()->getSmartyTemplate() . $dirname . $this->getClass().'/tpl/head.tpl') )
			$site['head'] = Config::create()->getSmartyTemplate() . $dirname . $this->getClass().'/tpl/head.tpl';

		$this->smarty->assign('site', $site);

		$this->smarty->display($dirname . $this->getClass().'/'.$this->getTemplate()->getNombre().'.html');
	}

	/**
	 * Retorna los datos de una plantilla
	 * @var string $html:		Html de la plantilla
	 * @var string $dirname:	Carpeta por defecto del template
	 */
	public function fetch($html, $dirname='')
	{
		if ( $dirname )
			$dirname .= '/';

		$this->smarty->assign('data', $this->smartyValues);

		return $this->smarty->fetch($dirname . $this->getClass().'/'.$html.'.html');
	}
}

?>