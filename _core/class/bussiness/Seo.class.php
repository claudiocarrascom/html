<?php

/**
 * Incluye los tags de SEO en el formato preestablecido
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class SeoTags
{
	private static $singleton;

	private $texto = '';

	private $title = '';

	private $description = '';

	private $keywords = '';

	/**
	 * Constructor
	 */
	public function __construct() {}

	/**
	 * Obtiene instancia de clase.
	 * @return SeoTags Instancia singleton
	 * @access public
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * Prepara el tag Title
	 * @param string $texto
	 */
	public function setTitle($texto)
	{
		$this->texto = trim($texto);

		//$this->setFormat();

		$texto = new String($texto);
		$texto->strToUpper();

		$this->title = $texto->getValue();
	}

	/**
	 * Prepara el tag Description
	 * @param string $texto
	 */
	public function setDescription($texto)
	{
		$this->texto = trim($texto);
		$this->texto = str_replace ('"', "'" ,$this->texto);
		$this->description = $this->texto;
	}

	/**
	 * Prepara el tag Keywords
	 * @param string $texto
	 */
	public function setKeywords($texto)
	{
		$this->texto = trim($texto);

		$this->removeAccents();

		$this->setFormat();

		$this->texto = preg_replace('/\s+/', ',', $this->texto);

		$this->texto = explode(',', $this->texto);

		$this->texto = array_unique($this->texto);

		$keywords = array();
		foreach( $this->texto as $word ) {
			if ( $word ) {
				$keywords[] = strtoupper($word); // MAYUSCULA
				if ( !is_numeric($word) ) {
					$keywords[] = strtolower($word); // minuscula
					$keywords[] = ucfirst(strtolower($word)); // Primera en Mayuscula
				}
			}
		}

		$this->keywords = implode(', ', $keywords);
	}

	/**
	 * Retorna el texto para el tag Title
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Retorna el texto para el tag Description
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Retorna el texto para el tag Keywords
	 * @return string
	 */
	public function getKeywords()
	{
		return $this->keywords;
	}


	/**
	 * Limpieza del texto
	 * @param string $texto:	Texto a procesar
	 * @return string
	 */
	private function wordsLimit($texto)
	{
		$texto = explode(' ', $texto);
		$words = array();
		foreach ( $texto as $value ) {
			if ( strlen($value) > 2 )
				$words[] = $value;
		}
		return implode(' ', $words);
	}

	/**
	 * Limpieza del texto
	 */
	private function setFormat()
	{
		$this->texto = html_entity_decode($this->texto);

		$this->texto = preg_replace('/^([^a-z0-9\s]+)$/i', '', $this->texto);
	}

	/**
	 * Elimina acentos, cremillas, etc
	 */
	private function removeAccents()
	{
		$this->texto = str_replace ( "'", '', $this->texto);
		$this->texto = str_replace ( '"', '', $this->texto);

		$this->texto = $this->cleanString($this->texto);
	}

	/**
	 * Reemplaza caracteres
	 * @param $txt
	 * @return mixed|string
	 */
	private function cleanString($txt)
	{
		$string = trim($txt);

		$string = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$string
		);

		$string = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$string
		);

		$string = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$string
		);

		$string = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$string
		);

		$string = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$string
		);

		$string = str_replace(
			array(/*'ñ', 'Ñ',*/ 'ç', 'Ç'),
			array(/*'n', 'N',*/ 'c', 'C',),
			$string
		);

		//Esta parte se encarga de eliminar cualquier caracter extraño
		$string = str_replace(
			array(
				'\\','¨','º','-','~','#','@','|','!','\"','·','$','%','&','/','(',')','?',"'",'¡','¿','[','^','`',']',
				'+','}','{','¨','´','>','< ','ª','°'
			),'',
			$string
		);

		return $string;
	}
}

?>