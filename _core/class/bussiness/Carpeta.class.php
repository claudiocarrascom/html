<?php

require_once (Config::create()->getCoreData().'Carpeta.class.php');

/**
 * Clase de Carpetas
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class Carpeta {

	private static $singleton;

	/**
	 * @var CarpetaDao
	 */
	private $dao;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->dao = new CarpetaDao();
	}

	/**
	 * Obtiene instancia de clase.
	 * @return Carpeta Instancia singleton
	 * @access public
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * Obtiene un registro de Carpeta
	 * @param int $id
	 * @return CarpetaVo|bool
	 */
	public function getCarpeta($id)
	{
		$response = false;
		if ( $this->dao->getCarpeta($id) )
			$response = $this->dao->getVO();

		return $response;
	}

	/**
	 * Devuelve las subcarpeta de un carpeta Raiz
	 * @param int $id:	Id de la carpeta raíz
	 * @return array
	 */
	public function getCarpetas($id)
	{
		$array = $this->dao->getCarpetas($id);

		$response = array();

		foreach( $array as $row ) {
			$response[] = array(
				'id'			=> $row->getId(),
				'carpeta'		=> $row->getCarpeta(),
				'url'			=> $row->getUrl(),
				'target'		=> $row->getTarget()
			);
		}

		return $response;
	}

	/**
	 * Devuelve las carpeta para el Menú Principal
	 * @return array
	 */
	public function getCarpetasMenu()
	{
		$array = $this->dao->getCarpetasMenu();

		$response = array();

		foreach( $array as $row ) {
			$response[] = array(
				'id'		=> $row->getId(),
				'carpeta'	=> $row->getCarpeta(),
				'url'		=> $row->getUrl(),
				'orden'		=> $row->getOrden(),
				'target'	=> $row->getTarget()
			);
		}

		return $response;
	}
}

?>