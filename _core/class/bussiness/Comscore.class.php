<?php

/**
 * Prepara las url para enviar a comScore
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class ComScore
{
	private static $singleton;

	private $site = '';

	private $id = '';

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->setId();

		$this->setSite();

}

	/**
	 * Obtiene instancia de clase.
	 * @return Comscore Instancia singleton
	 * @access public
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * Define el ID de Comscore
	 */
	public function setId()
	{
		$this->id = Config::create()->getComScoreId();
	}

	/**
	 * Define el nombre del Site en Comscore
	 */
	public function setSite()
	{
		$this->site = Config::create()->getComScoreSite();
	}

	/**
	 * Retorna el ID de Comscore
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Retorna el nombre clave del sitio en comscore
	 * @return string
	 */
	public function getSite()
	{
		return $this->site;
	}

	/**
	 * Obtiene el el parámetro "name" para enviar a Comscore
	 * @return $this
	 */
	public function getName()
	{
		$url = Request::create()->getUrlPath();

		$str = new String($url);

		if ( $str->getFirstChar()=='/' )
			$str->deleteFirstWord(1);

		if ( $str->findLastWord('.') !== false )
			$str->cutWord(0, $str->findLastWord('.'));

		$str->replaceWord('/', '.');

		$str->cleanUrl();

		if ( $str->getLastChar()=='.' )
			$str->cutWord(0, -1);

		return $str->getValue();
	}

	/**
	 * Obtiene el parámetro C3
	 * @return $this
	 */
	public function getC3()
	{
		if ( strpos($_SERVER['REQUEST_URI'], 'player/?id=')!==false ) {

			$id = substr($_SERVER['REQUEST_URI'],strripos($_SERVER['REQUEST_URI'], '=')+1,100);
			$articulo = Articulos::create()->getArticulo($id);

			if( strpos($articulo['url'], 'volverias-con-tu-ex/webshow/')!==false ) {

				$url = 'VOLVERIASCONTUEX_WEBSHOW';

			}elseif (strpos($articulo['url'], 'papa-a-la-deriva/diario-de-un-castigado/')!==false ) {

				$url = 'DIARIODEUNCASTIGADO';

			}else {

				$url = str_replace(Config::create()->getHttpBase(), '', $articulo['url']);

				$excludes = array('programas', 'teleseries');

				foreach ($excludes as $exclude) {
					$exclude .= '/';
					if (strpos($url, $exclude) !== false)
						$url = substr($url, strlen($exclude));
				}
			}

		}else{

			$url = Request::create()->getUrlPath();

			$excludes = array('programas', 'teleseries');

			foreach ( $excludes as $exclude ) {
				$exclude .= '/';
				if ( strpos($url, $exclude) !== false )
					$url = substr($url, strlen($exclude));
			}

		}

		$c3Url = explode('/', $url);
		$c3Url = str_replace('-', '', strtoupper($c3Url[0]));

		return $c3Url;

	}
}

?>