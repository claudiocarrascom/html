<?php

/**
 * Clase Date, manejo de fechas
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */

class Date {

	private static $singleton;

	private function __construct()
	{
		date_default_timezone_set('Chile/Continental');
	}

	/**
	 * Obtiene instancia de clase.
	 * @return Date Instancia singleton
	 * @access public
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * Retorna la fecha en el formato solicitado, utiliza los mismos tag de la función date de PHP
	 * @version v3.0 (20-01-11)
	 * @param string $fecha 	Fecha a formatear
	 * @param string $formato 	Formato de salida
	 * @param bool $nueva 		En caso de venir vacío el parámetro $fecha, indica si debe utilizar la de sistema
	 * @return datetime
	 */
	public static function formatDate($fecha, $formato, $nueva=true)
	{
		if ( empty($fecha) ) {
			if ( $nueva===true )
				$fecha = date('Y-m-d');
			else
				return '';
		}

		$fecha = trim($fecha);
		$salida = '';

		// Fecha con formato ISO
		if ( strpos($fecha,'T')!==false && strpos($fecha,'\\T')===false ) {
			$fecha = str_replace('T', ' ', $fecha);
		}

		// Separación de fecha y hora
		$hora = array();
		if ( strpos($fecha,' ')!==false ) {
			$hora = substr($fecha, strpos($fecha, ' ') + 1);
			$fecha = substr($fecha, 0, strpos($fecha, ' '));
			$hora = explode(':', $hora);
		}

		$date = array();
		if ( preg_match('/^(19[0-9]{2}|2[0-9]{3})[-\/]+(0?[0-9]|1[0-2])[-\/]+([0-2]?[0-9]|3[01])$/', $fecha, $aux) ) {
			$date[0] = $aux[1]; // Año
			$date[1] = $aux[2]; // Mes
			$date[2] = $aux[3]; // Día
		}
		elseif ( preg_match('/^([0-2]?[0-9]|3[01])[-\/]+(0?[0-9]|1[0-2])[-\/]+(19[0-9]{2}|2[0-9]{3})$/', $fecha, $aux) ) {
			$date[0] = $aux[3]; // Año
			$date[1] = $aux[2]; // Mes
			$date[2] = $aux[1]; // Día
		}
		else {
			$salida = $fecha;
			$formato = '';
		}

		// Si viene un formato, lo aplicamos
		if ( $formato ) {

			$fecha = array();
			$formato = explode(' ', $formato);

			foreach ( $formato as $format ) {

				$fecha[] = ' ';

				// Si el texto comienza con \ lo escribe completo hasta encontrar un espacio
				if ( $format[0]=='\\' ) {
					$fecha[] = str_replace('\\', '', $format);
				}
				else {

					// Aplicamos el formato indicado
					for ( $pos=0; $pos<strlen($format); $pos++ ) {

						// Si viene un \ entonces asignamos el siguiente caracter al formato de salida
						if ( $format[$pos]=='\\' ) {
							$pos++;
							$fecha[] = $format[$pos];
						}
						else {
							switch ( $format[$pos] ) {
								// Dias
								case 'd' :
									$fecha[] = (strlen($date[2])==1) ? '0'.$date[2] : $date[2];
									break;
								case 'D' :
									$shortDay = array('Dom','Lun','Mar','Mie','Jue','Vie','Sab');
									$fecha[] = $shortDay[date('w', mktime(0, 0, 0, $date[1], $date[2], $date[0]))];
									break;
								case 'l' :
									$longDay = array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');
									$fecha[] = $longDay[date('w', mktime(0, 0, 0, $date[1], $date[2], $date[0]))];
									break;

								// Meses
								case 'm' :
									$fecha[] = (strlen($date[1])==1) ? '0'.$date[1] : $date[1];
									break;
								case 'M' :
									$shortMonth = array('Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');
									$fecha[] = ($date[1]=='') ? $shortMonth[date('n')-1] : $shortMonth[$date[1]-1];
									break;
								case 'F' :
									$longMonth = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
									$fecha[] = ($date[1]=='') ? $longMonth[date('n')-1] : $longMonth[$date[1]-1];
									break;
								case 'n' :
									$fecha[] = preg_match('/^0/', $date[1]) ? $date[1][1] : $date[1];
									break;

								// Años
								case 'Y' :
									$fecha[] = $date[0];
									break;
								case 'y' :
									$fecha[] = substr($date[0], 2);
									break;

								// Hora
								case 'H' :
									$fecha[] = !isset($hora[0]) ? date('H') : $hora[0];
									break;
								case 'i' :
									$fecha[] = !isset($hora[1]) ? date('i') : $hora[1];
									break;
								case 's' :
									$fecha[] = !isset($hora[2]) ? date('s') : $hora[2];
									break;

								default:
									$fecha[] = $format[$pos];
							}
						}
					}
				}
			}
			$salida = trim(implode('', $fecha));
		}
		return $salida;
	}
}

?>