<?php

require_once (Config::create()->getCoreData().'Articulos.class.php');
require_once (Config::create()->getCoreData().'Multimedia.class.php');

/**
 * Manejo de Artículos
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */

class Articulos {

	private static $singleton;

	/**
	 * @var ArticulosDao
	 */
	public $dao;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->dao = new ArticulosDao();
	}

	/**
	 * Obtiene instancia de clase.
	 * @return Articulos Instancia singleton
	 * @access public
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * Retorna un registro de Artículo
	 * @param $id:	Id del artículo
	 * @return ArticuloVO|bool
	 */
	public function getArticulo($id)
	{
		$response = false;

		$row = $this->dao->getArticulo($id);

		if ( $row ) {

			$multimedia = new MultimediaDao();

			$videos = $multimedia->getMultimedia($row->getId(), 1);

			$galeria = $multimedia->getMultimedia($row->getId(), 2);

			//$documentos = $multimedia->getMultimedia($row->getId(), 3);

			$tags = array();
			foreach(explode(',', $row->getTags()) as $tag) {
				if ( $tag  = trim($tag) )
					$tags[] = $tag;
			}

			$response = array(
				'id'				=> $row->getId(),
				'idCarpeta'			=> $row->getIdRootCarpeta(),
				'carpeta'			=> $row->getCarpeta(),
				'idCarpetaArt'		=> $row->getIdCarpetaArt(),
				'titulo'			=> $row->getTitulo(),
				'bajada'			=> $row->getBajada(),
				'texto'				=> $row->getTexto(),
				'tags'				=> $tags,
				'fecha'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'd/m/Y'),
				'hora'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'H:i'),
				'url'				=> Config::create()->getHttpBase().$row->getUrlCarpeta() . $row->getUrl(),
				'urlcorta'			=> $row->getUrlCorta(),
				'relacionados'		=> $this->getRelacionados($row->getId()),
				'imagen'			=> $row->getUrlImagen() . $row->getImagen(),
				'urlCarpeta'		=> Config::create()->getHttpBase().$row->getUrlCarpeta(),
				'periodista'		=> $row->getPeriodista(),
				'imagenDescripcion'	=> $row->getImagenDescripcion()

			);

			// Asignación de video
			if ( count($videos) ) {
				$response['video'] = array(
					'nombre'		=> $videos[0]->getNombre(),
					'url'			=> $videos[0]->getUrl(),
					'tipo'			=> $videos[0]->getVideoTipo()
				);
			}

			// Asignación de galería
			if ( count($galeria) > 1 ) {
				$response['galeria'] = array();

				foreach( $galeria as $row ) {
					$response['galeria'][] = array(
						'nombre'		=> $row->getNombre(),
						'descripcion'	=> $row->getDescripcion(),
						'url'			=> $row->getUrl()
					);
				}
			}

			// Asignación de documentos
			/*if ( count($documentos) ) {
				$response['documento'] = $documentos[0]->getUrl() . $documentos[0]->getNombre();
			}*/
		}

		return $response;
	}

	/**
	 * Obtiene articulos no destacados de la Categoría
	 * @param int $carpeta:		Identificador de la carpeta
	 * @param int $page:		Número de página
	 * @param int $limit:		Cantidad de registros a retornar
	 * @param array $excludes:	Artículos excluidos de los resultados
	 * @return array
	 */
	public function getArticulos($carpeta, $page, $limit, $excludes=array())
	{
		$response = array();

		$articulos = array();
		foreach ( $excludes as $row )
			$articulos[] = $row['id'];

		if ( !$page )
			$page = 1;

		$array = $this->dao->getArticulos($carpeta, $page, $limit, $articulos);

		if ( $array ) {

			$data = array();

			foreach( $array['data'] as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				$data[] = array(
					'id'				=> $row->getId(),
					'idCarpeta'			=> $row->getIdCarpeta(),
					'titulo'			=> $row->getTitulo(),
					'bajada'			=> $row->getBajada(),
					'fecha'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'd/m/Y'),
					'hora'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'H:i'),
					'url'				=> $http . $row->getUrl(),
					'imagen'			=> $row->getUrlImagen() . $row->getImagen(),
					'video'				=> $row->getVideo(),
					'texto'				=> $row->getTexto(),
					'carpeta'			=> $row->getCarpeta(),
					'galeria'			=> $row->getImagenes() > 1 ? true : false
				);


			}
			$response['rows'] = $data;
			$response['total'] = (int) $array['total'];

			$response['page'] = (int) $page;
			$response['pages'] = (int) $array['pages'];
		}
		return $response;
	}

	/**
	 * Obtiene los artículos más vistos en un periodo
	 * @param int $interval:	Intervalo de tiempo en días
	 * @param int $limit:		Cantidad de registros requeridos
	 * @return array|bool
	 */
	public function getMasVistos($interval, $limit)
	{
		$response = false;

		$array = $this->dao->getMasVisitados($interval, $limit);

		if ( $array ) {

			$response = array();

			foreach( $array as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				$response[] = array(
					'titulo'			=> $row->getTituloAlternativo() ? $row->getTituloAlternativo() : $row->getTitulo(),
					'fecha'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'd/m/Y'),
					'hora'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'H:i'),
					'url'				=> $http . $row->getUrl(),
					'imagen'			=> $row->getUrlImagen() . $row->getImagen()
				);
			}
		}
		return $response;
	}

	/**
	 * Obtiene los artículos más vistos en un periodo de una carpeta
	 * @param int $interval:	Intervalo de tiempo en días
	 * @param int $limit:		Cantidad de registros requeridos
	 * @param int $idcarpeta:	Carpeta específica
	 * @return array|bool
	 */
	public function getMasVistosCarpeta($interval, $limit, $carpeta)
	{
		$response = false;

		$array = $this->dao->getMasVisitadosCarpeta($interval, $limit, $carpeta);

		if ( $array ) {

			$response = array();

			foreach( $array as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				$response[] = array(
					'titulo'			=> $row->getTituloAlternativo() ? $row->getTituloAlternativo() : $row->getTitulo(),
					'bajada'			=> $row->getBajada(),
					'fecha'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'd/m/Y'),
					'hora'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'H:i'),
					'url'				=> $http . $row->getUrl(),
					'imagen'			=> $row->getUrlImagen() . $row->getImagen(),
					'visitas'			=> $row->getVisitas(),
					'carpeta'			=> $row->getCarpeta()

				);
			}
		}
		return $response;
	}

	/**
	 * Obtiene los últimos artículos
	 * @param int $limit:	Cantidad de registros requeridos 10
	 * @return array|bool
	 */
	public function getUltimosArticulos($limit)
	{
		$response = false;

		$array = $this->dao->getUltimosArticulos($limit);

		if ( $array ) {

			$response = array();

			foreach( $array as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				$response[] = array(
					'titulo'			=> $row->getTituloAlternativo() ? $row->getTituloAlternativo() : $row->getTitulo(),
					'bajada'			=> $row->getBajada(),
					'fecha'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'd/m/Y'),
					'hora'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'H:i'),
					'url'				=> $http . $row->getUrl(),
					'imagen'			=> $row->getUrlImagen() . $row->getImagen()
				);
			}
		}
		return $response;
	}

	/**
	 * Obtiene los artículos relacionados de un artículo
	 * @param int $idArticulo:	Id del artículo
	 * @param string $type:		Tipo de relacionado (A=Artículo, V=Video, G=Galería)
	 * @return array|bool
	 */
	public function getRelacionados($idArticulo, $type='A')
	{
		$response = false;

		$array = $this->dao->getRelacionados($idArticulo, $type);

		if ( $array ) {

			$response = array();

			foreach( $array as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				$response[] = array(
					'id'			=> $row->getId(),
					'titulo'		=> $row->getTituloAlternativo() ? $row->getTituloAlternativo() : $row->getTitulo(),
					'bajada'		=> $row->getBajada(),
					'url'			=> $http . $row->getUrl(),
					'imagen'		=> $row->getUrlImagen() . $row->getImagen(),
					'video' 		=> $row->getVideo(),
					'carpeta'		=> $row->getCarpeta()
				);
			}
		}
		return $response;
	}

	/**
	 * Obtiene los resultados de una búsqueda artículos
	 * @param string $search:	Texto de búsqueda
	 * @param int $carpeta:		Id de Carpeta o NULL para busqueda sobre el sitio
	 * @param int $page:		Número de página
	 * @param int $limit:		Cantidad de registros a retornar
	 * @param int $type:		Tipo de búsqueda: search o tags
	 * @return array|bool
	 */
	public function getBusqueda($search, $carpeta, $page=1, $limit=10, $type)
	{
		$response = false;
		$search=trim($search);

		if ( strlen($search) > 2 ) {

			if ( !$page )
				$page = 1;

			$array = $this->dao->getBusqueda($search, $carpeta, $page, $limit, $type);
			if ( $array ) {

				$data = array();

				/**
				 * @var ArticuloVO $row
				 */
				foreach( $array['data'] as $row ) {

					$multimedia = new MultimediaDao();

					$docs = $multimedia->getMultimedia($row->getId(), 3);

					$http = '';
					if ( strpos($row->getUrl(), 'http://')===false )
						$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

					$data[] = array(
						'id'			=> $row->getId(),
						'titulo'		=> $row->getTituloAlternativo() ? $row->getTituloAlternativo() : $row->getTitulo(),
						'bajada'		=> $row->getBajada(),
						'fecha'			=> Date::create()->formatDate($row->getFechaPublicacion(), 'd/m/Y'),
						'url'			=> $http . $row->getUrl(),
						'imagen'		=> $row->getUrlImagen() . $row->getImagen(),
						'documento'		=> ( isset($docs[0]) ) ? $docs[0]->getUrl() . $docs[0]->getNombre() : '',
					);
				}

				$response['rows'] = $data;
				$response['total'] = (int) $array['total'];

				$response['page'] = (int) $page;
				$response['pages'] = (int) $array['pages'];
			}
		}
		return $response;
	}

	/**
	 * Obtiene los artículos para Galerías
	 * @return array|bool
	 */
	public function getGalerias($limit)
	{
		$response = false;

		$array = $this->dao->getGalerias($limit);

		if ( $array ) {

			$response = array();

			foreach( $array as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				$response[] = array(
					'id'			=> $row->getId(),
					'titulo'		=> $row->getTituloAlternativo() ? $row->getTituloAlternativo() : $row->getTitulo(),
					'url'			=> $http . $row->getUrl(),
					'imagen'		=> $row->getUrlImagen() . $row->getImagen()
				);
			}
		}
		return $response;
	}

	/**
	 * Obtiene los artículos de una Carpeta específica para Fotogalería
	 * @return array|bool
	 */
	public function getGaleriaCarpeta($limit, $idCarpeta)
	{
		$response = false;

		$array = $this->dao->getGaleriaCarpeta($limit, $idCarpeta);

		if ( $array ) {

			$response = array();

			foreach( $array as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				$response[] = array(
					'id'			=> $row->getId(),
					'titulo'		=> $row->getTituloAlternativo() ? $row->getTituloAlternativo() : $row->getTitulo(),
					'url'			=> $http . $row->getUrl(),
					'imagen'		=> $row->getUrlImagen() . $row->getImagen()
				);
			}
		}
		return $response;
	}

	/**
	 * Obtiene articulos destacados por Categoría
	 * @param int $carpeta:		Identificador de la carpeta
	 * @param int $limit:		Limite de resultados
	 * @return array
	 */
	public function getDestacados($carpeta, $limit)
	{
		$response = array();

		$array = $this->dao->getDestacados($carpeta, $limit);

		if ( $array ) {

			$response = array();

			foreach( $array as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				// Prepara artículos relacionados
				$relacionados = false;
				if ( $row->getPosicion()==1 )
					$relacionados = $this->getRelacionados($row->getId());

				// Prepara formato de salida de la fecha y hora
				$fecha = Date::create()->formatDate($row->getFechaPublicacion(), 'H:i');
				if ( Date::create()->formatDate($row->getFechaPublicacion(), 'Ymd') < date('Ymd') )
					$fecha = Date::create()->formatDate($row->getFechaPublicacion(), 'd/m/Y');

				$response[$row->getPosicion()] = array(
					'id'				=> $row->getId(),
					'idCarpeta'			=> $row->getIdRootCarpeta(),
					'titulo'			=> $row->getTituloAlternativo() ? $row->getTituloAlternativo() : $row->getTitulo(),
					'bajada'			=> $row->getBajada(),
					'fecha'				=> $fecha,
					'hora'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'H:i'),
					'url'				=> $http . $row->getUrl(),
					'imagen'			=> $row->getUrlImagen() . $row->getImagen(),
					'video'				=> $row->getVideo(),
					'videoTipo'			=> $row->getVideoTipo(),
					'relacionados'		=> $relacionados,
					'seccion'			=> $row->getSeccion(),
					'carpeta'			=> $row->getCarpeta(),
					'galeria'			=> $row->getImagenes() > 1 ? true : false
				);
			}
		}
		return $response;
	}

	/**
	 * Obtiene articulos adyacentes a partir de un artículo y carpeta específicos
	 * @param int $id:		Identificador de artículo
	 * @param int $carpeta:		Identificador de la carpeta
	 * @param int $curso:		Identificador de dirrecion, PRE O POST
	 * @param int $limit:		Limite de resultados
	 * @return array
	 */
	public function getArticulosAdyacentes($id, $carpeta, $curso, $limit)
	{
		$response = false;

		$array = $this->dao->getArticulosAdyacentes($id, $carpeta, $curso, $limit);

		if ( $array ) {

			$response = array();

			foreach( $array as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				$response[] = array(
					'titulo'			=> $row->getTituloAlternativo() ? $row->getTituloAlternativo() : $row->getTitulo(),
					'bajada'			=> $row->getBajada(),
					'fecha'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'd/m/Y'),
					'hora'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'H:i'),
					'url'				=> $http . $row->getUrl(),
					'imagen'			=> $row->getUrlImagen() . $row->getImagen(),
					'visitas'			=> $row->getVisitas(),
					'carpeta'			=> $row->getCarpeta()

				);
			}
		}
		return $response;
	}

	/**
	 * Obtiene por id carpeta padre, el último artículo públicados de cada carpeta hijo
	 * @param int $carpeta:	Identificador de la carpeta
	 * @param int $limit:	Cantidad de registros requeridos 15
	 * @return array|bool
	 */
	public function getUltimosArticulosCarpeta($carpeta, $limit)
	{
		$response = false;

		$array = $this->dao->getUltimosArticulosCarpeta($carpeta, $limit);

		if ( $array ) {

			$response = array();

			foreach( $array as $row ) {

				$http = '';
				if ( strpos($row->getUrl(), 'http://')===false )
					$http = Config::create()->getHttpBase().$row->getUrlCarpeta();

				$response[] = array(
					'id'				=> $row->getId(),
					'titulo'			=> $row->getTitulo(),
					'bajada'			=> $row->getBajada(),
					'fecha'				=> Date::create()->formatDate($row->getFechaPublicacion(), 'd/m/Y'),
					'url'				=> Config::create()->getHttpBase().$row->getUrlCarpeta(),
					'texto'				=> $row->getTexto(),
					'imagen'			=> $row->getUrlImagen() . $row->getImagen(),
					'carpeta'			=> $row->getCarpeta()
				);
			}
		}
		return $response;
	}

	/**
	 * Retorna un registro de Artículo
	 * @param int $id:			ID del artículo requerido
	 * @param int $idcarpeta:	ID de la carpeta
	 * @return ArticuloVO|bool
	 */
	public function getArticuloVo($id, $idcarpeta=null)
	{
		$response = false;

		$row = $this->dao->getArticulo($id, $idcarpeta);
		if ( $row )
			$response = $row;

		return $response;
	}

	function object_to_array($obj) {

		$tags = array();
		foreach(explode(',', $obj->getTags()) as $tag) {
			if ( $tag  = trim($tag) )
				$tags[] = $tag;
		}

		$response = array(
			'id'				=> $obj->getId(),
			'idCarpeta'			=> $obj->getIdCarpeta(),
			'idRootCarpeta'		=> $obj->getIdRootCarpeta(),
			'idCarpetaArt'		=> $obj->getIdCarpetaArt(),
			'idTemplate'		=> $obj->getIdTemplate(),
			'idClass'			=> $obj->getIdClase(),
			'carpeta'			=> $obj->getCarpeta(),
			'titulo'			=> $obj->getTitulo(),
			'tituloAlt'			=> $obj->getTituloAlternativo(),
			'bajada'			=> $obj->getBajada(),
			'texto'				=> $obj->getTexto(),
			'tags'				=> $obj->getTags(),
			'fecha'				=> Date::create()->formatDate($obj->getFechaPublicacion(), 'd/m/Y'),
			'hora'				=> Date::create()->formatDate($obj->getFechaPublicacion(), 'H:i'),
			'url'				=> $obj->getUrl(),
			'urlcorta'			=> $obj->getUrlCorta(),
			'periodista'		=> $obj->getPeriodista(),
			'urlCarpeta'		=> $obj->getUrlCarpeta(),
			'relacionados'		=> $this->getRelacionados($obj->getId()),
			'urlCarpeta'		=> $obj->getUrlCarpeta(),
			'periodista'		=> $obj->getPeriodista(),
			'imagen'			=> $obj->getUrlImagen().$obj->getImagen(),
			'imagenDescripcion'	=> $obj->getImagenDescripcion(),
			'imagenes'			=> $obj->getImagenes(),
			'imgUrl'			=> $obj->getUrlImagen(),
			'estado'			=> $obj->getestado(),
		);

		$multimedia = new MultimediaDao();

		$videos = $multimedia->getMultimedia($obj->getId(), 1);

		$galeria = $multimedia->getMultimedia($obj->getId(), 2);

		// Asignación de video
		if ( count($videos) ) {
			$response['video'] = array(
				'nombre'		=> $videos[0]->getNombre(),
				'url'			=> $videos[0]->getUrl(),
				'tipo'			=> $videos[0]->getVideoTipo()
			);
		}

		// Asignación de galería
		if ( count($galeria) > 1 ) {
			$response['galeria'] = array();

			foreach( $galeria as $row ) {
				$response['galeria'][] = array(
					'nombre'		=> $row->getNombre(),
					'descripcion'	=> $row->getDescripcion(),
					'url'			=> $row->getUrl()
				);
			}
		}

		return $response;

	}


}

?>