<?php

/**
 * Recupera parámetros enviados por URL, de tipo POST ó GET
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class Request {

	private static $singleton;

	private $POST;

	private $GET;

	private $url;

	private $urlPath;

	private $urlDirname;

	/**
	 * Constructor
	 */
	private function __construct()
	{
		$this->POST	= &$_POST;
		$this->GET	= &$_GET;
		$this->url	= &$_SERVER['REQUEST_URI'];

		$this->setUrlPath();
	}

	/**
	 * Obtiene instancia de clase.
	 * @return Request Instancia singleton
	 * @access public
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * @return bool
	 */
	public function isRequest()
	{
		return ( count($this->POST) > 0 || count($this->GET) > 0 ) ? true : false;
	}

	/**
	 * Valida que se envíe un formulario desde el mismo servidor
	 * @return bool
	 */
	public function isValidRequest()
	{
		$response = true;

		/*print_r($_SERVER);

		if ( $_SERVER['SERVER_NAME']!='localhost' ) {
			if ( $_SERVER['SERVER_NAME']==$_SERVER['SERVER_ADDR'] )
				$response = true;
		}
		else
			$response = true;*/

		return $response;
	}

	/**
	 * @param $param
	 * @return mixed
	 */
	public function getPost($param)
	{
		return $this->POST[$param];
	}

	/**
	 * @param $param
	 * @return mixed
	 */
	public function getParam($param)
	{
		$response = '';
		if ( isset($this->GET[$param]) )
			$response = $this->cleanGET($this->GET[$param]);

		return $response;
	}

	/**
	 * Obtiene la porción de url para verificar mapeo
	 * @return void
	 */
	public function setUrlPath()
	{
		$pathParts = parse_url($this->url);

		$url = new String(Config::create()->getHttpRoot().$pathParts['path']);
		$url->replaceWord(Config::create()->getHttpBase(), '');

		/**
		 * Check si es raiz del sitio
		 */
		$redirect = '';
		$urlPath = '';

		if ( !$url->getSize() ) {

			$redirect = Config::create()->getHttpHome();
		}
		else {

			// Es artículo
			if ( $url->findOneWord('.html') || $url->findOneWord('.xml') ) {
				$urlPath = $url->getValue();
			}
			else {
				// Es carpeta
				if ( $url->getLastChar()=='/' )
					$urlPath = $url->getValue();
				else
					$redirect = $url->getValue().'/';
			}
		}

		if ( $redirect != '' ) {
			header('Location:'.$redirect);
		}
		else {
			$this->urlPath = $urlPath;
		}
	}

	/**
	 * @return string
	 */
	public function getUrlDirname()
	{
		return $this->urlDirname;
	}

	/**
	 * @return string
	 */
	public function getUrlPath()
	{
		return $this->urlPath;
	}

	/**
	 * Previene XSS y SQLi
	 * @param $string
	 * @return string
	 */
	public function cleanGET($string)
	{
		$string = strip_tags($string);
		$string = htmlspecialchars($string);

		$patron = array('select ','insert ','delete ','drop ','truncate ','alter ','table ','from ', 'sleep', ' and ', ' or ', 'like ');
		$string = str_ireplace($patron, '', $string);

		$patron = array(';', '=', '%', '--', '^', '[', ']', '\\', '!', '¡', '?', '&', '*');
		$string = str_ireplace($patron, '', $string);

		$string = addslashes($string);

		return trim($string);
	}
}

?>