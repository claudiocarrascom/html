<?php

require_once (Config::create()->getCoreData().'Url.class.php');

/**
 * Valida las url a las que se está accediendo y incrementa su contador de visitas
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class Url {

	private static $singleton;

	/**
	 * @var UrlDao
	 */
	private $dao;

	/**
	 * Constructor
	 */
	private function __construct()
	{
		$this->dao = new UrlDao();
	}

	/**
	 * Obtiene instancia de clase.
	 * @return Url Instancia singleton
	 * @access public
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * Chequea que sea una URL válida
	 * @return bool
	 */
	public function check()
	{
		$response = false;

		if ( $this->dao->isValid(Request::create()->getUrlPath()) )			
			$response = true;		
		return $response;
	}

	/**
	 * Retorna un objeto de tipo UrlVo
	 * @return UrlVo
	 */
	public function getUrlVO()
	{
		return $this->dao->getVO();
	}
}

?>