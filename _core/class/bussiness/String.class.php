<?php

/**
 * File String.class,
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class String {

	/**
	 * @var private;
	*/
	private $valor;

	/**
	 * @uses setValor para limpiar el string
	 * @param string $valor
	 * @access public
	*/
	public function __construct($valor)
	{
		$this->valor = $valor;
	}

	/**
	 * @uses subclass::isString para comprobar si es un String
	 * @param string $valor
	 * @access public
	*/
	private function setValor($valor)
	{
		$this->valor = $this->trim($valor);
	}

	/**
	 * @uses trim() para limpiar los bordes del String
	 * @param string $valor
	 * @access public
	*/
	public function trim($valor)
	{
		return trim($valor);
	}

	/**
	 * @return $this->valor tamaño del String
	 * @access public
	*/
	public function getValue()
	{
		return $this->valor;
	}

	/**
	 * @uses Strlen() para capturar el tamaño del String
	 * @return int tamaño del String
	 * @access public
	 */
	public function getSize()
	{
		return strlen($this->valor);
	}

	/**
	 * @return string
	 */
	public function getLastChar()
	{
		return substr($this->valor, -1);
	}

	/**
	 * @return string
	 */
	public function getFirstChar()
	{
		return substr($this->valor, 0, 1);
	}

	/**
	 * @param int $numberDelete
	 * @uses substr() para eliminar los primeros $numberDelete caracteres.
	 * @access public
	*/
	public function deleteFirstWord($numberDelete)
	{
		$this->valor = substr($this->valor, $numberDelete);
	}

	/**
	 * @uses preg_match(), para busca una palabra en un String
	 * @uses subclass::setValor()
	 * @uses subclass::deleteSeparator()
	 * @return bolean , si encuentra la palabra
	 * @access public
	*/
	public function findOneWord($word)
	{
		$this->setValor($this->valor);

		if( preg_match("/$word/i",$this->valor )) {
			return true;
		}else {
			return false;
		}
	}

	/**
	 * @uses strrpos(), Busca la última apareción de una cadena
	 * @access public
	 * @param String $search
	 * @return int
	 */
	public function findLastWord($search)
	{
		return strrpos($this->valor, $search);
	}

	/**
	 * @uses substr(), Corta un string segun posiciones indicadas
	 * @access public
	 * @param int $ini, int $fin
	 */
	public function cutWord($ini, $fin)
	{
		$this->valor = substr($this->valor, $ini, $fin);
	}

	/**
	 * @uses str_replace(), para reemplazar una palabra por otra
	 * @param String #search, String $repacle,
	 * @access public
	*/
	public function replaceWord($search, $replace)
	{
		$this->valor = str_replace($search, $replace, $this->valor);
	}

	/**
	 * @uses strtr(), para reemplazar caracteres a mayúsculas
	 * @access public
	 */
	public function strToUpper()
	{
		$string = strtoupper($this->valor);

		$string = str_replace(
			array('á', 'à', 'ä', 'â'),
			array('Á', 'À', 'Â', 'Ä'),
			$string
		);

		$string = str_replace(
			array('é', 'è', 'ë', 'ê'),
			array('É', 'È', 'Ê', 'Ë'),
			$string
		);

		$string = str_replace(
			array('í', 'ì', 'ï', 'î'),
			array('Í', 'Ì', 'Ï', 'Î'),
			$string
		);

		$string = str_replace(
			array('ó', 'ò', 'ö', 'ô'),
			array('Ó', 'Ò', 'Ö', 'Ô'),
			$string
		);

		$string = str_replace(
			array('ú', 'ù', 'ü', 'û', ),
			array('Ú', 'Ù', 'Ü', 'Û'),
			$string
		);

		$string = str_replace(
			array('ñ', 'ç'),
			array('Ñ', 'Ç'),
			$string
		);

		$this->valor = $string;
	}

	/**
	 * Limpia un string de caracteres raros para la url
	 */
	public function cleanUrl()
	{
		$patron = array(
			'/à|á|â|ã|ä|å|æ|ª/'	=> 'a',
			'/À|Á|Â|Ã|Ä|Å|Æ/'	=> 'A',
			'/è|é|ê|ë|ð/'		=> 'e',
			'/È|É|Ê|Ë|Ð/'		=> 'E',
			'/ì|í|î|ï/'			=> 'i',
			'/Ì|Í|Î|Ï/'			=> 'I',
			'/ò|ó|ô|õ|ö|ø|º/'	=> 'o',
			'/Ò|Ó|Ô|Õ|Ö|Ø/'		=> 'o',
			'/ù|ú|û|ü/'			=> 'u',
			'/Ù|Ú|Û|Ü/'			=> 'U',

			'/ç/'				=> 'c',
			'/Ç/'				=> 'C',
			'/ý|ÿ/'				=> 'y',
			'/Ý|Ÿ/'				=> 'Y',
			'/ñ/'				=> 'n',
			'/Ñ/'				=> 'N',
			'/þ/'				=> 't',
			'/Þ/'				=> 'T',
			'/ß/'				=> 's'
		);

		$this->valor = preg_replace(array_keys($patron), array_values($patron), $this->valor);

		$this->valor = preg_replace(array('/\s/','/[^a-z0-9\-]/i'), array('-', ''), $this->valor);
	}

	/**
	 * Crea un url a partir de un string
	 * @return string
	 */
	public function createUrl()
	{
		$this->valor = $this->trim($this->valor);

		$this->cleanUrl();

		$this->valor = strtolower($this->valor);

		$this->valor = preg_replace('/\-+/', '-', $this->valor);

		return $this->valor;
	}
}

?>