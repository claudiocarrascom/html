<?php

/**
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
Class FLogger {

	private static $singleton;

	private function __construct()
	{
		$configure = array(
			'rootLogger' => array(
				'appenders' => array('default'),
			),
			'appenders' => array(
				'default' => array(
					'class' => 'LoggerAppenderFile',
					'layout' => array(
						'class' => 'LoggerLayoutSimple'
					),
					'params' => array(
						'file' => Config::create()->getServerLog().Config::create()->getLogFile().date('_Y_m_d').'.log',
						'append' => true
					)
				)
			)
		);

		Logger::configure($configure);
	}

	/**
	 * Obtiene instancia de clase.
	 * @return Flogger Instancia singleton
	 * @access public
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * @param Exception $error
	 */
	public function error($error)
	{
		$logger = Logger::getLogger('logger');
		$logger->error(date('d-m-Y H:i:s').' - '.$error);
	}

	public function getLogger()
	{
		return Logger::getLogger('logger');
	}
}

?>