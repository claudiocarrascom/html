<?php

/**
 * Clase que maneja la inclusión de archivos
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */

class Includes
{
	private static $singleton;

	/**
	 * Constructor
	 */
	private function __construct()
	{
		// Class generadora de Logs
		require_once(Config::create()->getCorePlugins().'Log4php/src/main/php/Logger.php');

		$this->includesAdodb();

		$this->includesClass();

		$this->includesPlugins();
	}

	/**
	 * Obtiene instancia de clase.
	 * @return Includes Instancia singleton
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}
	
	/**
	 * Muestra los errores en modo debug
	 * @example ?ctrl=1
	 */
	public function showErrors()
	{
		error_reporting(0);

		if ( isset($_GET['ctrl']) && $_GET['ctrl']==1 ) {
			ini_set('display_errors', 1);
			error_reporting(E_ALL);
		}
	}	
	
	/**
	 * Includes de Adodb
	 */
	private function includesAdodb()
	{
		try {
			if ( !@include_once(Config::create()->getCoreData().'common/Adodb.class.php') )
				throw new Exception ('Adodb.class.php no existe');
		}
		catch( Exception $e ) {
			FLogger::create()->error($e);
		}
	}

	/**
	 * Includes de Clases
	 */
	private function includesClass()
	{
		$class = array(
			'Flogger',
			'Url',
			'String',
			'Request',
			'Date',
			'Seo',
			'Comscore',
			'Module',
			'Carpeta',
			'Articulos'
		);

		try {
			foreach( $class as $file )
				if ( !@include_once(Config::create()->getCoreBussiness().'/'.$file.'.class.php') )
					throw new Exception ($file.'.class.php no existe');
		}
		catch( Exception $e ) {
			FLogger::create()->error($e);
		}
	}

	/**
	 * Includes de Plugins
	 */
	private function includesPlugins()
	{
		$plugins = array(
			'Mobile-Detect-2.8.11/Mobile_Detect',
			'Smarty-3.1.19/libs/Smarty.class'
		);

		try {
			foreach( $plugins as $file )
				if ( !@include_once(Config::create()->getCorePlugins().$file.'.php') )
					throw new Exception ($file.'.php no existe');
		}
		catch( Exception $e ) {
			FLogger::create()->error($e);
		}
	}

	/**
	 * Chequea que el archivo a incluir exista
	 * @param $file
	 * @throws Exception
	 * @return bool
	 */
	public function checkIncludes($file)
	{
		try {
			if ( !@include_once($file) )
				throw new Exception ('No existe archivo solicitado: '.$file);

			return true;
		}
		catch( Exception $e ) {
			FLogger::create()->error($e);
			return false;
		}
	}
}

?>