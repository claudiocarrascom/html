<?php

/**
 * Modelo de datos de Template
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class TemplateVO {

	private $id;

	private $nombre;

	/**
	 * Constructor
	 */
	public function __construct() {}

	/**
	 * Define el ID del Template
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Retorna el ID del Template
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Define el nombre del Template
	 * @param string $nombre
	 */
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}

	/**
	 * Retorna el nombre del Template
	 * @return string
	 */
	public function getNombre()
	{
		return $this->nombre;
	}
}

?>