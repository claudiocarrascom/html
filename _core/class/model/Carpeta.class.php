<?php

/**
 * Modelo de datos de Carpeta
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class CarpetaVO {

	private $id;

	private $idPadre;

	private $nombre;

	private $url;

	private $target = '_self';

	private $orden;

	private $parentsUrl;

	private $titulo;

	private $descripcion;

	private $keywords;

	private $config;

	private $estado;

	/**
	 * @var TemplateVO
	 */
	private $templateVO;

	/**
	 * @var ClaseVO
	 */
	private $claseVO;


	/**
	 * Constructor
	 */
	public function __construct() {}

	/**
	 * Define el ID de la Carpeta
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Retorna el ID de la Carpeta
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Define el ID de la Carpeta Padre
	 * @param int $idPadre
	 */
	public function setIdPadre($idPadre)
	{
		$this->idPadre = $idPadre;
	}

	/**
	 * Retorna el ID de la Carpeta Padre
	 * @return int
	 */
	public function getIdPadre()
	{
		return $this->idPadre;
	}

	/**
	 * Define el nombre de la Carpeta
	 * @param string $nombre
	 */
	public function setCarpeta($nombre)
	{
		$this->nombre = $nombre;
	}

	/**
	 * Retorna el nombre de la Carpeta
	 * @return string
	 */
	public function getCarpeta()
	{
		return $this->nombre;
	}

	/**
	 * Define la url de la Carpeta
	 * @param string $url
	 */
	public function setUrl($url)
	{
		if (strpos($url, 'http://')===false) {
			$this->url = Config::create()->getHttpBase() . $url;
			$this->target = '_self';
		}else{
			$this->url = $url;
			$this->target = '_blank';
		}
	}

	/**
	 * Retorna la url de la Carpeta
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Retorna el target de la Carpeta
	 * @return string
	 */
	public function getTarget()
	{
		return $this->target;
	}

	/**
	 * Define orden de la Carpeta
	 * @param string $url
	 */
	public function setOrden($orden)
	{
		$this->orden = $orden;
	}

	/**
	 * Retorna orden de la Carpeta
	 * @return string
	 */
	public function getOrden()
	{
		return $this->orden;
	}

	/**
	 * Define la url completa de la Carpeta, incluida la de los Padres
	 * @param string $url
	 */
	public function setParentUrl($url)
	{
		$this->parentsUrl = $url;
	}

	/**
	 * Retorna la url completa de la Carpeta
	 * @return string
	 */
	public function getParentUrl()
	{
		return $this->parentsUrl;
	}

	/**
	 * Define el título de la Carpeta
	 * @param string $titulo
	 */
	public function setTitulo($titulo)
	{
		$this->titulo = $titulo;
	}

	/**
	 * Retorna el título de la Carpeta
	 * @return string
	 */
	public function getTitulo()
	{
		return $this->titulo;
	}

	/**
	 * Define la descripción de la Carpeta
	 * @param string $descripcion
	 */
	public function setDescripcion($descripcion)
	{
		$this->descripcion = $descripcion;
	}

	/**
	 * Retorna la descripción de la Carpeta
	 * @return string
	 */
	public function getDescripcion()
	{
		return $this->descripcion;
	}

	/**
	 * Define las keywords de la Carpeta
	 * @param string $keywords
	 */
	public function setKeywords($keywords)
	{
		$this->keywords = $keywords;
	}

	/**
	 * Retorna las keywords de la Carpeta
	 * @return string
	 */
	public function getKeywords()
	{
		return $this->keywords;
	}

	/**
	 * Define el config de la Carpeta
	 * @param string $keywords
	 */
	public function setConfig($config)
	{
		$this->config = json_decode($config, false);
	}

	/**
	 * Retorna el config de la Carpeta
	 * @return string
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * Define el estado de la Carpeta
	 * @param string $estado
	 */
	public function setEstado($estado)
	{
		$this->estado = $estado;
	}

	/**
	 * Retorna el estado de la Carpeta
	 * @return string
	 */
	public function getEstado()
	{
		return $this->estado;
	}

	/**
	 * Define un objeto de tipo TemplateVO
	 * @param TemplateVO
	 */
	public function setTemplateVO($TemplateVO)
	{
		$this->templateVO = $TemplateVO;
	}

	/**
	 * Retorna un objeto de tipo TemplateVO
	 * @return TemplateVO
	 */
	public function getTemplateVO()
	{
		return $this->templateVO;
	}

	/**
	 * Define un objeto de tipo ClaseVO
	 * @param ClaseVO
	 */
	public function setClaseVO($ClaseVO)
	{
		$this->claseVO = $ClaseVO;
	}

	/**
	 * Retorna un objeto de tipo ClaseVO
	 * @return ClaseVO
	 */
	public function getClaseVO()
	{
		return $this->claseVO;
	}
}

?>