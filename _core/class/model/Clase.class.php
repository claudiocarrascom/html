<?php

/**
 * Modelo de datos de Clases
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class ClaseVO {

	private $id;

	private $nombre;

	/**
	 * Constructor
	 */
	public function __construct() {}

	/**
	 * Define el ID de la Clase
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Retorna el ID de la Clase
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Define el nombre de la Clase
	 * @param $nombre
	 */
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}

	/**
	 * Retorna el nombre de la Clase
	 * @return mixed
	 */
	public function getNombre()
	{
		return $this->nombre;
	}
}

?>