<?php

/**
 * Modelo de datos de Url
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class UrlVO {

	private $id;

	/**
	 * @var string $tipo
	 * @example 1:Carpeta, 2:Articulo
	 */
	private $tipo;

	private $idCarpeta;

	private $carpeta;

	private $idPadre;

	/**
	 * Constructor
	 */
	public function __construct() {}

	/**
	 * Define el ID de la Url
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Retorna el ID de la Url
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Define el tipo de URl
	 * @param string $tipo
	 */
	public function setTipo($tipo)
	{
		$this->tipo = $tipo;
	}

	/**
	 * Retorna el tipo de URl
	 * @return string
	 */
	public function getTipo()
	{
		return $this->tipo;
	}

	/**
	 * Define la carpeta
	 * @param int $carpeta
	 */
	public function setIdCarpeta($idCarpeta)
	{
		$this->idCarpeta = $idCarpeta;
	}

	/**
	 * Retorna la carpeta
	 * @return string
	 */
	public function getIdCarpeta()
	{
		return $this->idCarpeta;
	}

	/**
	 * Retorna la carpeta
	 * @return string
	 */
	public function getCarpeta()
	{
		return $this->carpeta;
	}

	/**
	 * Define la carpeta
	 * @param String $carpeta
	 */
	public function setCarpeta($carpeta)
	{
		$this->carpeta = $carpeta;
	}

	/**
	 * Define la carpeta Padre
	 * @param int $idPadre
	 */
	public function setIdPadre($idPadre)
	{
		$this->idPadre = $idPadre;
	}

	/**
	 * Retorna la carpeta Padre
	 * @return int
	 */
	public function getIdPadre()
	{
		return $this->idPadre;
	}

	/**
	 * Define la url completa de la Carpeta, incluida la de los Padres
	 * @param string $url
	 */
	public function setParentUrl($url)
	{
		$this->parentsUrl = $url;
	}

	/**
	 * Retorna la url completa de la Carpeta
	 * @return string
	 */
	public function getParentUrl()
	{
		return $this->parentsUrl;
	}

	/**
	 * Define nombre de Template
	 * @param int $nombreTemplate
	 */
	public function setTemplateVo($templateVo)
	{
		$this->templateVo = $templateVo;
	}

	/**
	 * Retorna nombre de la Template
	 * @return string
	 */
	public function getTemplateVo()
	{
		return $this->templateVo;
	}

	/**
	 * Define un objeto de tipo ClaseVO
	 * @param ClaseVO
	 */
	public function setClaseVO($ClaseVO)
	{
		$this->claseVO = $ClaseVO;
	}

	/**
	 * Retorna un objeto de tipo ClaseVO
	 * @return ClaseVO
	 */
	public function getClaseVO()
	{
		return $this->claseVO;
	}
}

?>