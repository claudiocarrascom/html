<?php

/**
 * Modelo de datos de Multimedia
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class MultimediaVO {

	private $id;

	private $idArticulo;

	private $tipo;

	private $nombre;

	private $nombreOriginal;

	private $descripcion;

	private $videoTipo;

	private $url;

	private $crc;

	private $orden;

	private $estado;

	/**
	 * Constructor
	 */
	public function __construct() {}

	/**
	 * Sobrecarga del setField
	 * @param $var
	 * @param $value
	 */
	public function set($var, $value)
	{
		$this->$var = $value;
	}

	/**
	 * Retorna el ID del registro
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Retorna el ID del artículo relacionado
	 * @return int
	 */
	public function getIdArticulo()
	{
		return $this->idArticulo;
	}

	/**
	 * Retorna el tipo de archivo: 1:video - 2:Imagen
	 * @return string
	 */
	public function getTipo()
	{
		return $this->tipo;
	}

	/**
	 * Retorna el nombre del archivo
	 * @return string
	 */
	public function getNombre()
	{
		return $this->nombre;
	}

	/**
	 * Retorna el nombre original del archivo
	 * @return string
	 */
	public function getNombreOriginal()
	{
		return $this->nombreOriginal;
	}

	/**
	 * Retorna el directorio donde se encuentra el archivo
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Retorna el tipo de video
	 * @return string
	 */
	public function getVideoTipo()
	{
		return $this->videoTipo;
	}

	/**
	 * Retorna la descripción del archivo
	 * @return string
	 */
	public function getDescripcion()
	{
		return $this->descripcion;
	}

	/**
	 * Retorna el número de orden en el que debe aparecer
	 * @return int
	 */
	public function getOrden()
	{
		return $this->orden;
	}

	/**
	 * Retorna el valor CRC
	 * @return int
	 */
	public function getCRC()
	{
		return $this->crc;
	}

	/**
	 * Retorna el estado de publicación 0:No, 1:Si
	 * @return int
	 */
	public function getEstado()
	{
		return $this->estado;
	}
}

?>