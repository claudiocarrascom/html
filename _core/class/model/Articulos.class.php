<?php

/**
 * Modelo de datos de Articulos
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class ArticuloVO {

	private $id;

	private $idCarpeta;

	private $idRootCarpeta;

	private $idCarpetaArt;

	private $idTemplate;

	private $idClass;

	private $carpeta;

	private $titulo;

	private $tituloAlternativo;

	private $bajada;

	private $texto;

	private $fechaCreacion;

	private $fechaPublicacion;

	private $tags;

	private $url;

	private $urlCorta;

	private $periodista;

	private $imagenDescripcion;

	private $urlCarpeta;

	private $urlAlternativa;

	private $imgNombre;

	private $imgUrl;

	private $imgUrlSrv;

	private $videoNombre;

	private $videoTipo;

	private $imagenes;

	private $posicion;

	private $visitas;

	private $vigencia;

	private $estado;

	private $seccion;

	/**
	 * @var TemplateVO
	 */
	private $templateVO;

	/**
	 * @var ClaseVO
	 */
	private $claseVO;

	/**
	 * @var MultimediaVO[]
	 */
	private $multimedia;

	/**
	 * Constructor
	 */
	public function __construct() {}

	/**
	 * Sobrecarga del setField
	 * @param $var
	 * @param $value
	 */
	public function set($var, $value)
	{
		$this->$var = $value;
	}

	/**
	 * Retorna el ID del artículo
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Retorna el ID de la carpeta
	 * @return int
	 */
	public function getIdCarpeta()
	{
		return $this->idCarpeta;
	}

	/**
	 * Retorna el ID de la carpeta del artículo
	 * @return int
	 */
	public function getIdCarpetaArt()
	{
		return $this->idCarpetaArt;
	}

	/**
	 * Retorna el ID de la carpeta ROOT de la sección
	 * @return int
	 */
	public function getIdRootCarpeta()
	{
		return $this->idRootCarpeta;
	}

	/**
	 * Retorna el ID del Template
	 * @return int
	 */
	public function getIdTemplate()
	{
		return $this->idTemplate;
	}

	/**
	 * Retorna el ID de la Clase
	 * @return int
	 */
	public function getIdClase()
	{
		return $this->idClass;
	}

	/**
	 * Retorna el título de la carpeta
	 * @return string
	 */
	public function getCarpeta()
	{
		return $this->carpeta;
	}

	/**
	 * Retorna el título del artículo
	 * @return string
	 */
	public function getTitulo()
	{
		return stripslashes($this->titulo);
	}

	/**
	 * Retorna el título corto o alternativo del artículo
	 * @return string
	 */
	public function getTituloAlternativo()
	{
		return stripslashes($this->tituloAlternativo);
	}

	/**
	 * Retorna la bajada del artículo
	 * @return string
	 */
	public function getBajada()
	{
		return stripslashes($this->bajada);
	}

	/**
	 * Retorna el cuerpo del artículo
	 * @return string
	 */
	public function getTexto()
	{
		return html_entity_decode($this->texto);
	}

	/**
	 * Retorna los tags del artículo
	 * @return string
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 * Retorna la fecha de creación
	 * @return mixed
	 */
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}

	/**
	 * Retorna la fecha de publicación
	 * @return mixed
	 */
	public function getFechaPublicacion()
	{
		return $this->fechaPublicacion;
	}

	/**
	 * Retorna la url del artículo o la alternativa en el caso de tenerla
	 * @return string
	 */
	public function getUrl()
	{
		return $this->urlAlternativa ? $this->urlAlternativa : $this->url;
	}

	/**
	 * Retorna la url corta del artículo
	 * @return string
	 */
	public function getUrlCorta()
	{
		return $this->urlCorta;
	}

	/**
	 * Retorna el nombre del periodista
	 * @return string
	 */
	public function getPeriodista()
	{
		return $this->periodista;
	}

	/**
	 * Retorna la descripcion de la imagen
	 * @return string
	 */
	public function getImagenDescripcion()
	{
		return $this->imagenDescripcion;
	}

	/**
	 * Retorna la url de la carpeta
	 * @return string
	 */
	public function getUrlCarpeta()
	{
		return $this->urlCarpeta;
	}

	/**
	 * Retorna el nombre de la imagen
	 * @return string
	 */
	public function getImagen()
	{
		return $this->imgNombre;
	}

	/**
	 * Retorna la ruta a la imagen
	 * @return string
	 */
	public function getUrlImagen()
	{
		return $this->imgUrl;
	}

	/**
	 * Retorna la ruta a la imagen server
	 * @return string
	 */
	public function getUrlImagenSrv()
	{
		return $this->imgUrlSrv;
	}

	/**
	 * Retorna el nombre del video
	 * @return string
	 */
	public function getVideo()
	{
		return $this->videoNombre;
	}


	/**
	 * Retorna el tipo del video
	 * @return string
	 */
	public function getVideoTipo()
	{
		return $this->videoTipo;
	}

	/**
	 * Retorna la cantidad de imágenes del artículo
	 * @return string
	 */
	public function getImagenes()
	{
		return $this->imagenes;
	}

	/**
	 * Retorna la cantidad de visitas
	 * @return int
	 */
	public function getVisitas()
	{
		return (int) $this->visitas;
	}

	/**
	 * Retorna la posicion en el destacado
	 * @return int
	 */
	public function getPosicion()
	{
		return $this->posicion;
	}

	/**
	 * Retorna la vigencia del artículo concurso
	 * @return mixed
	 */
	public function getVigencia()
	{
		return $this->vigencia;
	}

	/**
	 * Retorna el estado actual del artículo
	 * @return mixed
	 */
	public function getEstado()
	{
		return $this->estado;
	}

	/**
	 * Retorna el nombre de programa o sección principal de la nota
	 * @return mixed
	 */
	public function getSeccion()
	{
		return $this->seccion;
	}

	/**
	 * Define un objeto de tipo TemplateVO
	 * @param TemplateVO
	 */
	public function setTemplateVO($TemplateVO)
	{
		$this->templateVO = $TemplateVO;
	}

	/**
	 * Retorna un objeto de tipo TemplateVO
	 * @return TemplateVO
	 */
	public function getTemplateVO()
	{
		return $this->templateVO;
	}

	/**
	 * Define un objeto de tipo ClaseVO
	 * @param ClaseVO
	 */
	public function setClaseVO($ClaseVO)
	{
		$this->claseVO = $ClaseVO;
	}

	/**
	 * Retorna un objeto de tipo ClaseVO
	 * @return ClaseVO
	 */
	public function getClaseVO()
	{
		return $this->claseVO;
	}

	public function getMultimedia()
	{
		return $this->multimedia;
	}
}

?>