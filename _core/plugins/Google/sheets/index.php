<?php

// Composer's auto-loading functionality
//require "../vendor/autoload.php";
require_once(Config::create()->getCorePlugins().'Google/vendor/autoload.php');
 
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
 
//
/*
 * Aquí van los datos de la sección credenciales del proyecto que creamos con Google Console
 * Modificarlos según su propia direccion de correo y Id de cliente
 */

function getIndiceTelefonicoGoogle()
{
 $nombreAplicacion = "spreadsheet-php";

 //$direccionCorreo = "oscar-615@spreadsheet-php.iam.gserviceaccount.com";
 $direccionCorreo = "desarrollo-mega@spreadsheet-php-1181.iam.gserviceaccount.com";

 //$idCliente = "69564883783-5dn2tk3ife7335586nm3jkprc40iuu8r.apps.googleusercontent.com";
 $idCliente = "911580932601-hgami4hg9afupnbm3hbb10cn2l1ph61f.apps.googleusercontent.com";

 //$key_file_location = Config::create()->getCorePlugins() . 'Google/sheets/Spreadsheet_php-a2c95b14c115.p12';
 $key_file_location = Config::create()->getCorePlugins() . 'Google/sheets/Spreadsheet-php-0bbf6349e393.p12';

 // Nombre del SpreadSheet creada
 $nombreSpreahSheet = "Firma Corporativa";
 // Nombre de hoja de cálculo
 $hojaCalculo = "Data";

 $scope = 'https://spreadsheets.google.com/feeds';

 // Inicializamos Google Client
 $client = new Google_Client();

 $client->setApplicationName($nombreAplicacion);
 $client->setClientId($idCliente);

 $key = file_get_contents($key_file_location);

 // credenciales, scope y archivo p12. Agregar el correcto Path al archivo p12
 $cred = new Google_Auth_AssertionCredentials(
     $direccionCorreo,
     array($scope),
     $key
 );

 $client->setAssertionCredentials($cred);

 // si expiro el access token generamos otro
 if ($client->getAuth()->isAccessTokenExpired()) {
  $client->getAuth()->refreshTokenWithAssertion($cred);
 }

 // Obtenemos el access token
 $obj_token = json_decode($client->getAccessToken());
 $accessToken = $obj_token->access_token;

 // Inicializamos google-spreadsheet-client
 $serviceRequest = new DefaultServiceRequest($accessToken);
 ServiceRequestFactory::setInstance($serviceRequest);

 //Obtenemos los Spreadsheets disponibles para las credenciales actuales
 $spreadsheetService = new Google\Spreadsheet\SpreadsheetService();

 $spreadsheetFeed = $spreadsheetService->getSpreadsheets();

 // Obtenemos la spreadsheet por su nombre
 $spreadsheet = $spreadsheetFeed->getByTitle($nombreSpreahSheet);

 // Obtenemos las hojas de cálculo de la spreadsheet obetenida
 $worksheetFeed = $spreadsheet->getWorksheets();

 // Obtenemos la hoja de cálculo por su nombre
 $worksheet = $worksheetFeed->getByTitle($hojaCalculo);
 $listFeed = $worksheet->getListFeed();

 foreach ($listFeed->getEntries() as $entry) {
  $values[] = $entry->getValues();
 }


 return $values;

}

function objectToArray( $object )
{
 if( !is_object( $object ) && !is_array( $object ) )
 {
  return $object;
 }
 if( is_object( $object ) )
 {
  $object = get_object_vars( $object );
 }
 return array_map( 'objectToArray', $object );
}

/*
 * Array de datos a agregar.
 * Observar que el valor de la claves del array que representan los encabezados
 * de las columnas van en minúsculas, en vez de Email sería email.
 * Esto es porque los encabezados de columna deben coincidir exactamente
 * con lo que fue devuelto por la API de Google y no por lo que se ve en Google Drive.
 */
 
 /*$dataAgregar = array('nombre' => 'Juan',
 'email' => 'Martinez',
 'telefono' => '1567890'
 );*/
// Agregar datos
//$listFeed->insert($dataAgregar);

?>