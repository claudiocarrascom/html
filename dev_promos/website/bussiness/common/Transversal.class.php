<?php

/**
 * Clase con funciones transversales del Sitio
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class Transversal
{
	private static $singleton;

	/**
	 * Constructor
	 */
	private function __construct()
	{

	}

	/**
	 * Obtiene instancia de clase.
	 * @return Transversal Instancia singleton
	 * @access public
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * Define variables generales del Header
	 * @param Module $class
	 */
	public function setHeader($class)
	{
		$class->setValue('facebook', Config::create()->getFacebookUrl());

		$class->setValue('twitter', Config::create()->getTwitterUrl());

		$class->setValue('youtube', Config::create()->getYoutubeUrl());

		$class->setValue('instagram', Config::create()->getInstagramUrl());

		$class->setValue('date', date('d/m/Y'));

		$class->setValue('hour', date('H:i'));

		$class->setValue('currentDate', Date::create()->formatDate(date('Y-m-d'), 'd \de F'));

		$class->setValue('carpeta', $class->getVo()->getCarpeta());
	}

	/**
	 * Define archivos javascript propios de la sección
	 * @param string|array $js:		Nombre del archivo JS
	 * @param Module $class
	 */
	public function setJavascript($js, $class)
	{
		$files = $js;
		if ( !is_array($js) )
			$files = array($js);

		$javascript = array();
		foreach($files as $file) {
			if ( strpos($file, '/')!==false )
				$javascript[] = substr($file, strpos($file, '/') + 1);
			else
				$javascript[] = $file;
		}

		if ( count($javascript) )
			$class->setValue('javascript', $javascript);
	}

	public function setRss($file)
	{
		require_once (Config::create()->getCorePlugins() . 'XML2Array/Xml2array.class.php');

		$data = false;

		// Archivo RSS
		$fileXml = Config::create()->getWebsiteTemplates().'_static/_common/xml/'.$file;

		if ( !$data ) {
			$fo = fopen($fileXml, 'r');
			if ( $fo ) {
				$data = fread($fo, filesize($fileXml));
				fclose($fo);
			}
		}

		$data = xml2array($data);

		$response = false;
		if ( isset($data['rss']['channel']) )
			$response = $data['rss']['channel'];

		$array =  $response['item'];

		return $array;
	}
}

?>