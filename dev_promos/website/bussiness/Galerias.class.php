<?php

/**
 * Galerías de Mega
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class Galerias extends Module
{
	/**
	 * Despliegue de Galería
	 */
	public function galeria()
	{
		$id = (int) $_GET['galeria'];

		// Artículo de la Galería
		$articulo = Articulos::create()->getArticulo($id);

		// SEO Google
		$this->setTitle('GALERIA: ' . $articulo['titulo']);
		$this->setDescription($articulo['bajada']);
		$this->setKeywords($articulo['tags']);

		$this->setValue('articulo', $articulo);

		// Galerías
		$this->setValue('nextGalerias', array_reverse(Articulos::create()->getGalerias(10)));
		
		$this->display();
	}
}

?>