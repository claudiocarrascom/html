<?php

/**
 * Home del sitio MEGA
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class Home extends Module
{
	/**
	 * Método para el despliegue de la Home del sitio
	 */
	public function index()
	{
		$this->transversal();
		
		$this->setValue('msg', 'Hola Mundo !!!');
		
		$this->display();
	}

	/**
	 * Método para el despliegue de la Sección del sitio
	 */
	public function seccion()
	{
		$this->transversal();

		// Nombre de la sección
		$this->setValue('carpeta', $this->getVo()->getCarpeta());

		// Paginador
		$this->setValue('paginador', $this->getVo()->getParentUrl().'?');

		$page = 1;
		if (Request::create()->getParam('page'))
			$page = (int) Request::create()->getParam('page');

		print_r(Articulos::create()->getArticulos($this->getId(), $page, 10));
		// Artículos
		$this->setValue('articulos', Articulos::create()->getArticulos($this->getId(), $page, 10));

		$this->display();
	}

	/**
	 * Método para el despliegue de los Artículos del sitio
	 */
	public function articulo()
	{
		$this->transversal();

		// Artículo
		$this->setValue('articulo', Articulos::create()->getArticulo($this->getId()));

		$this->display();
	}
	
	/**
	 * Método para el despliegue del Buscador del sitio
	 */
	public function buscador()
	{
		$this->transversal();

		// Búsqueda
		if ( Request::create()->isValidRequest() ) {

			$page = (int) Request::create()->getParam('page');

			$param = Request::create()->getParam('q');

			$this->setValue('texto', $param);

			// Paginador
			$this->setValue('paginador', $this->getVo()->getParentUrl().'?q='.$param.'&');

			if ( Request::create()->getParam('q')!='' )
				$this->setValue('articulos', Articulos::create()->getBusqueda(Request::create()->getParam('q'), null, $page, 15, 'search'));
		}

		$this->display();
	}

	/**
	 * Método para el despliegue 404 del sitio
	 */
	public function _404()
	{
		if(isset($_GET['p']))
			$url = $_GET['p'];

		$this->transversal();

		//url procedencia
		$this->setValue('url_procedencia', $url);

		$this->display();
	}
	
	/**
	 * Tranversal de la Home
	 */
	private function transversal()
	{
		$transversal = Transversal::create();

		$transversal->setHeader($this);
	}
}

?>