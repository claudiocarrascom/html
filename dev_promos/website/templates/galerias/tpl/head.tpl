
	<link href="{$site.urlStatic}galerias/css/style.css" rel="stylesheet">

	<script src="{$site.urlResources}js/royalslider/jquery.royalslider.min.js"></script>
	<script src="{$site.urlStatic}galerias/js/galerias.js?d=08102015" defer></script>

	{literal}
		<script type="text/javascript" language="JavaScript">
			function fbShare(url, title, descr, image, winWidth, winHeight) {
				var winTop = (screen.height / 2) - (winHeight / 2);
				var winLeft = (screen.width / 2) - (winWidth / 2);
				window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + encodeURI(title) + '&p[summary]=' + descr + '&p[url]=' + encodeURI(url) + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
			}
			function twShare(url, title, descr, image, winWidth, winHeight) {
				var winTop = (screen.height / 2) - (winHeight / 2);
				var winLeft = (screen.width / 2) - (winWidth / 2);
				window.open('https://twitter.com/intent/tweet?original_referer=' + encodeURI(url) + '&text=' + encodeURI(title) + '&tw_p=tweetbutton&url=' + encodeURI(url) , 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
			}
		</script>
	{/literal}