$(document).ready(function(){
	var firstSlide = $("#gallery .rsSlideBlock").first();
	var imgUrl = firstSlide.find('a').attr('href');
	console.log(imgUrl);
	firstSlide.clone().addClass("first").prependTo(".royalSlider");
	$(".first .bg-cover").css('background-image', 'url(' + imgUrl + ')');

	$('#gallery').royalSlider({
		fullscreen: {
			enabled: true,
			nativeFS: true
		},
		controlNavigation: 'thumbnails',
		loop: false,
		fadeinLoadedSlide: true,
		imageScaleMode: 'fit',
		navigateByClick: false,
		numImagesToPreload: 3,
		arrowsNav: true,
		arrowsNavAutoHide: true,
		keyboardNavEnabled: true,
		globalCaption: true,
		globalCaptionInside: true,
		deeplinking: {
			enabled: true,
			change: true,
			prefix: 'images/'
		},
		thumbs: {
			appendSpan: true,
			firstMargin: true,
			paddingBottom: 4
		}
	});
	$('#gallery').addClass('fullHeight');
	$('#gallery').removeClass('no-visible');

	// Next & Back galería
	var sliders = jQuery('.royalSlider').data('royalSlider');
	
	sliders.ev.on('rsBeforeMove', function(e, type, action) {
		//console.log(sliders.currSlideId, type, sliders.numSlides);
		if(sliders.currSlideId == sliders.numSlides-1 && type == 'next') {
			if (typeof($('#nextUrl').val()) === "undefined") {
				//algo				
			} else {
				location.href = $('#nextUrl').val();
			}
		}		
	});

	// Tracking de Analytics
	var slider = $(".royalSlider").data('royalSlider');
	slider.ev.on('rsAfterSlideChange', function() {
		gaTracking(document.location.href);
	});

	

	$(window).load(function(){
     $('#gallery').royalSlider('updateSliderSize', true);
	});
});