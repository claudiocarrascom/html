//PARA SKIN RANDOM
function randomSkin(bgImageList, bgUrl){
	var images = bgImageList;
	$('body').css({'background-image': 'url(' + bgUrl + images[Math.floor(Math.random() * images.length)] + ')'});
};


$(document).ready(function() {
	var megaHero = $('.mega-hero-slider');
	if ( megaHero.length ) {
		megaHero.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 600,
			fade: true,
			lazyLoad: 'ondemand',
			arrows: false,
			dots: true,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 4000,
			pauseOnDotsHover: true,
			responsive: [{
				breakpoint: 1000,
				settings: {
					arrows: false
				}
			},{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					autoplaySpeed: 6000,
					slidesToScroll: 1
				}
			}]
		});
	}
	//Activa menu touch
	var setMenu = 1;

	var resizeNavM = function(){
	 var altoMenu = ($(".mega-main-head").outerHeight(true));
	 var altoPopUp = ($(".header-overlay").height()) - altoMenu - 30;
	 $(".mobile-lb-menu").css("height", altoPopUp);
	};

	$(".menu-toggle").click(function(){
	 //calcula alto para el menu
	 resizeNavM();

	 if(setMenu){
	   $("body").toggleClass("active-menu");
	   setTimeout(function() {
	       $("body").toggleClass("menu-is-ready");
	   }, 201);
	   setMenu = 0;
	 }else{
	   $("body").toggleClass("menu-is-ready");
	   setTimeout(function() {
	       $("body").toggleClass("active-menu");
	   }, 201);
	   setMenu = 1;
	 };

	});
	/*muestra oculta el buscador*/
	$(".toggle-search").click(function(){
		$(".toggle-search-wrap").toggleClass("visible");
		$(".toggle-search").toggleClass("active");
		if ($(".toggle-search-wrap").hasClass("visible")) {
    		$(".input-home").focus();
		} else {
    		$(".input-home").blur();
		}
	});
	//Cambiar a fixed menu
	var navDistTop = $('div.mega-main-head-wrap').outerHeight(true);
	function cambiarAfixed(objetivo) {
		if ($(window).scrollTop() > (navDistTop+100))
			$(objetivo).addClass('fixed');
		else
			$(objetivo).removeClass('fixed');
	}
	var timeoutNav;
	$(window).scroll(function(){
		/*oculta el buscador*/
		if ($(".toggle-search-wrap").hasClass("visible")){
			$(".toggle-search-wrap").toggleClass("visible");
			$(".toggle-search").toggleClass("active");
			$(".input-home").blur();
		};

		clearTimeout(timeoutNav);
    	timeoutNav = setTimeout(function(){
	        resizeNavM();
	   }, 200);
		cambiarAfixed('div.mega-main-head-wrap');
	});

	// Compartir en redes sociales
	function shareUrl(theUrl){
		var top = ($(window).height()/2) - 225;
		var left = ($(window).width()/2) - 275;
		window.open(theUrl, "_blank", '"toolbar=no, scrollbars=yes, resizable=yes, top=' + top + ', left=' + left + ', width=550, height=450"');
	};
	$('.share-button.fb').click(function(e){
		var title = encodeURIComponent($('.media-video .article-header .title').text());
		var locationSafe = encodeURIComponent(window.location.href);
		var url = 'http://www.facebook.com/share.php?u=' + locationSafe + '&title=' + title;
		shareUrl(url);
		e.preventDefault();
	});

	 $('.share-button.tw').click(function(e){
		var title = encodeURIComponent($('.media-video .article-header .title').text() + ' |');
		var locationSafe = encodeURIComponent(window.location.href);
		var url = 'http://twitter.com/intent/tweet?status='+title+'+'+locationSafe;
		shareUrl(url);
		e.preventDefault();
	});
	// Funcion abrir galerias
	function popUpGallery(){
		$('.small-gallery').on('click', '.gal-link', function() {
    		var selfhref = $(this).attr("href");
    		window.open( selfhref,"_blank","toolbar=yes, location=yes, directories=no, status=no, menubar=yes, scrollbars=yes, copyhistory=yes");
			return false;
		});
	};
	// Slider pequeño para las galerías
	function smallSlider(target, arrowsParent){
    $(target).slick({
            slidesToShow: 5,
            slidesToScroll: 5,
            speed: 600,
            fade: false,
            lazyLoad: 'ondemand',
            arrows: true,
            dots: false,
            infinite: true,
            appendArrows: $(arrowsParent + ' .small-gallery-buttons'),
            responsive: [
                {
                  breakpoint: 1000,
                  settings: {
                    arrows: true
                  }
                },
                {
                  breakpoint: 700,
                  settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6,
                    arrows: false
                  }
                },
                {
                  breakpoint: 460,
                  settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    arrows: false
                  }
                },
                {
                  breakpoint: 350,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    arrows: false
                  }
                }
            ]
          });
  };
	if($('.recomended .small-slider').length){
		smallSlider(".recomended .small-slider", ".recomended");
		popUpGallery();
	}
	if($('.article-gal .small-slider').length){
		smallSlider(".article-gal .small-slider", ".article-gal");
		popUpGallery();
	}

	// Color random
	var classes = ["yellow", "purple", "blue", "pink", "green"];
	function randomColor(target){
		target.each(function(){
			$(this).addClass(classes[~~(Math.random()*classes.length)]);
		});
	}
	randomColor($(".mega-article .media-video"));

	//Tabs de mega
  function tabsMega(tabName, tabActive, buttonActive){
    var $theButton = $("div.mega-tab-buttons[data-tabname='" + tabName + "'] .p-nav-link");
    var $theTab = $("div.mega-tab-content[data-tabtarget='" + tabName + "'] .mega-tab-item");
    $theTab.removeClass("active");
    $theTab.eq(tabActive).addClass("active");
    $theButton.eq(buttonActive).addClass("current");
    $theButton.click(function(e){
      $theTab.removeClass("active");
      $theButton.removeClass("current");
      var $this = $(this);
      var targetTabNumber = $(this).attr("data-tab-number");
      $this.addClass('current');
      $theTab.eq(targetTabNumber).addClass("active");
      e.preventDefault();
    });
  };

  tabsMega("tab-m-vistos", 0, 0);

  //Activa parrilla según día
  var thisDay = (new Date().getDay());
  tabsMega("tab-parrilla", thisDay, thisDay-1);

    // Definicion de current de seccion
	$('.p-nav-link, .nav-link').each( function(pos, tag) {
		var url = document.location.href.split('?');

		if (tag == url[0])
			$(tag).addClass('current');
	});

	// Definicion de current de articulo
	$('.program-nav, .p-nav-link').each( function(pos, tag) {
		var url = document.location.href.split('/');
		var exp = document.location.href.replace(url[url.length-1], '');

		if (exp == tag)
			$(tag).addClass('current');
	});

	/*//interacción nav mobile
	var activeTabMobile = function(){
		var title = $(".p-nav-link.current").text();
		console.log(title);
		$(".program-nav-toggle span").html(title);
	};
	activeTabMobile();*/

	$(".program-nav-toggle").click(function(){
	   $(this).next('.program-nav').toggle().toggleClass("active");
	});
	$('.program-nav-wrap').on('click', '.program-nav.active', function(){
	 $(this).toggle().toggleClass("active");
	});

	// Funciones señal en vivo
	randomColor($("div.bubble-block"));
});