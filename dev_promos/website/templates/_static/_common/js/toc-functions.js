$(document).ready(function(){
	//Activa menu touch
	var setMenu = 1;
	$(".menu-toggle").click(function(){
	 //calcula alto para el menu
	 if(setMenu){
	   $("body").toggleClass("active-menu");
	   setTimeout(function() {
	       $("body").toggleClass("menu-is-ready");
	   }, 201);
	   setMenu = 0;
	 }else{
	   $("body").toggleClass("menu-is-ready");
	   setTimeout(function() {
	       $("body").toggleClass("active-menu");
	   }, 201);
	   setMenu = 1;
	 };
	});
	/*muestra oculta el buscador*/
	$(".toggle-search").click(function(){
		$(".toggle-search-wrap").toggleClass("visible");
		$(".toggle-search").toggleClass("active");		
		if ($(".toggle-search-wrap").hasClass("visible")) {
    		$(".input-home").focus();
		} else {
    		$(".input-home").blur();
		}
	});

	$(window).scroll(function(){
		/*oculta el buscador*/
		if ($(".toggle-search-wrap").hasClass("visible")){
			$(".toggle-search-wrap").toggleClass("visible");
			$(".toggle-search").toggleClass("active");
			$(".input-home").blur();
		};
	});
});