
$(document).ready(function() {

	// Cambia el alto de las cajas segun horario
	var altoBurbujas = function(){
		$('.bubble-list').each(function(){
			var $this = $(this);
			var count = 0;
			var maxLength = $this.find("li.bubble-item").length;
			while(count < maxLength){
				var $theBubble = $this.find("div.bubble-block");
				var $theHour = $this.find("span.program-hour");
				var first =  $theHour.eq(count).text().replace("00:", "24:").replace("01:", "25:").replace("02:", "26:").split(':').join("");
				var second = $theHour.eq(count+1).text().replace("00:", "24:").replace("01:", "25:").replace("02:", "26:").split(':').join("");
				var fecha = Math.abs((parseInt(first)) - (parseInt(second)));
				$theBubble.eq(count).css("height", fecha * 0.5);
				//console.log(first, second, fecha, fecha*0.5)
				count++;
			}
		});
	};
	altoBurbujas();

	// Interval para refrescar nombres de programas
	/*var setInterval = function() {
		var url = $('#url').val();
		if ( typeof(url) !== 'undefined' ) {
			if ( url=='vivo/' ) {
				goToList(url, $('#id').val());
			}
		}
	};*/
});