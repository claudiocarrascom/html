$(document).ready(function(){
	//Calcula el alto del header en el medium slide
	var calcMediumHeader = function(){
		$(".medium-hidden").each(function(){
			var alto = $(this).find("h3").outerHeight(true);
			$(this).css('height', alto);
		});
	};
	calcMediumHeader();
	$(window).load(function(){
		calcMediumHeader();
	});
	$(window).resize(function(){
		setTimeout(function() {
	       calcMediumHeader();
	   }, 500);		
	});

	//Slider AHN & EM
 	$(".big-slide").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 600,
		fade: true,
		lazyLoad: 'ondemand',
		arrows: false,
		dots: true,
		autoplay: true,
		autoplaySpeed: 7000,
		infinite: true,
		responsive: [{
			breakpoint: 1599,
			settings: {
			slidesToShow: 1,
			slidesToScroll: 1
			}
		},{
			breakpoint: 1024,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: false
			},
		}]
	});
	//Slider Mediano, galerías, lo + visto
	function mediumSlider(target, arrowsParent, XLBreak, LBreak, MBreak, SBreak, XMBreak){
		$(target).slick({
			slidesToShow: XLBreak,
			slidesToScroll: XLBreak,
			speed: 600,
			fade: false,
			lazyLoad: 'ondemand',
			arrows: true,
			dots: false,
			infinite: true,
			appendArrows: $(arrowsParent + ' .small-gallery-buttons'),
			responsive: [{
				breakpoint: 1599,
				settings: {
					arrows: true,
					slidesToShow: LBreak,
					slidesToScroll: LBreak
				}
			},
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: MBreak,
					slidesToScroll: MBreak,
					swipeToSlide: true
				}
			},
			{
				breakpoint: 700,
				settings: {
					slidesToShow: SBreak,
					slidesToScroll: SBreak,
					swipeToSlide: true,
					centerMode: true,
					centerPadding: '40px',
					arrows: false
				}
			},
			{
				breakpoint: 420,
				settings: {
					slidesToShow: XMBreak,
					slidesToScroll: XMBreak,
					swipeToSlide: true,
					centerMode: true,
					centerPadding: '40px',
					arrows: false
				}
			}]
		});
	}
	if($('.medium-slide-1 .medium-slider').length){
		mediumSlider(".medium-slide-1 .medium-slider", ".medium-slide-1", 3, 3, 2, 2, 1);
	}
	if($('.medium-slide-2 .medium-slider').length){
		mediumSlider(".medium-slide-2 .medium-slider", ".medium-slide-2", 4, 3, 3, 2, 1);
	}
});