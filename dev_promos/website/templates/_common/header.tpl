<div class="mega-main-width mega-main-head-wrap">
	<header class="mega-main-head">
		{if !($site.isSmartphone)}
			<a href="{$site.urlBase}" class="go-home"><img src="{$site.urlStatic}_common/images/logos/logo-mega-animado.gif" class="mega-logo" width="90" height="116"></a>
		{/if}
		<div class="menu-toggle vert-center">
			<img src="{$site.urlStatic}_common/images/logos/mega-white-small.png" class="mega-logo" width="80" height="83">
			<div class="mega-toggle-nav">
				<span class="bar-top"></span>
				<span class="bar-mid"></span>
				<span class="bar-bot"></span>
				<p class="menu-text vert-center"></p>
			</div>
		</div>
		<div class="mobile-lb-menu nav-wrap vert-center">
			<nav class="center-content">
				<ul class="text-buttons">
					<li class="nav-item"><a href="{$site.urlBase}" class="nav-link">Home</a></li>
					<li class="nav-item"><a href="{$site.urlBase}exclusivo/" class="nav-link">Imperdibles</a></li>
					<li class="nav-item"><a href="{$site.urlBase}programas/" class="nav-link">Programas</a></li>
					<li class="nav-item"><a href="{$site.urlBase}teleseries/" class="nav-link">Teleseries</a></li>
					<li class="nav-item"><a href="{$site.urlBase}lo-mas-de-youtube/" class="nav-link">Lo <i>+</i> de YouTube</a></li>
					<li class="nav-item"><a href="{$site.urlBase}mega-eventos/" class="nav-link">Eventos</a></li>
					<li class="nav-item"><a href="http://tramites.ahoranoticias.cl/" target="_blank" class="nav-link">Mega Trámites</a></li>
					<li class="nav-item"><a href="http://www.ahoranoticias.cl" target="_blank" class="nav-link">Noticias</a></li>
				</ul>

				<ul class="capsulas">
					<li class="nav-item live-item m25-item"><a href="{$site.urlBase}25/" target="_blank" class="nav-link">25 Años Mega</a></li>
					<li class="nav-item live-item la-roja-item"><a href="{$site.urlBase}la-roja/" target="_blank" class="nav-link"><span>Sitio oficial de</span> la Roja</a></li>
					<li class="nav-item live-item cap-completos"><a href="{$site.urlBase}capitulos-completos/teleseries/" class="nav-link">Capítulos Completos</a></li>
					<li class="nav-item live-item"><a href="{$site.urlBase}espectaculos/" class="nav-link">Espectáculos</a></li>
					<li class="nav-item live-item espect-item"><a href="{$site.urlBase}causas/" class="nav-link">Mi Causa mi MEGA</a></li>
					<li class="nav-item live-item"><a href="{$site.urlBase}senal-en-vivo/" class="nav-link"><span>Señal</span> en Vivo</a></li>
				</ul>

				<ul class="social-wrap">
					<li class="social-item"><a href="{$data.facebook}" class="social-link fb" target="_blank">Facebook</a></li>
					<li class="social-item"><a href="{$data.twitter}" class="social-link tw" target="_blank">Twitter</a></li>
					<li class="social-item"><a href="{$data.instagram}" class="social-link ig" target="_blank">Instagram</a></li>
					<li class="social-item"><a href="{$data.youtube}" class="social-link yt" target="_blank">YouTube</a></li>
				</ul>
			</nav>
		</div>
		<!--en vivo mobile-->
		<div class="vert-center show-on-mobile-nav">
			<a href="{$site.urlBase}senal-en-vivo/" class="mobile-nav-link">En Vivo</a>
		</div>
		<!--buscador boton-->
		<div class="vert-center nav-right">
			<span class="circle-button search-icon toggle-search"></span>
			<a href="{$site.urlBase}buscador/" class="circle-button search-icon mobile-search-button"></a>
		</div>
		<!--buscador-->
		<div class="toggle-search-wrap vert-center">
			<form method="get" action="{$site.urlBase}buscador/" class="buscador-home">
				<input type="text" placeholder="Buscar" value="" name="q" class="input-home">
				<input type="submit" onclick="this.form.submit();return false;" value="" name="buscar" class="search-button-home">
			</form>
		</div>
		<!--redes sociales-->
		<div class="vert-center nav-right social-toggler">
			<span class="circle-button social-icon">Redes Sociales</span>
			<ul class="social-wrap toc-socials">
				<li class="social-item"><a href="{$data.facebook}" class="social-link fb" target="_blank">Facebook</a></li>
				<li class="social-item"><a href="{$data.twitter}" class="social-link tw" target="_blank">Twitter</a></li>
				<li class="social-item"><a href="{$data.instagram}" class="social-link ig" target="_blank">Instagram</a></li>
				<li class="social-item"><a href="{$data.youtube}" class="social-link yt" target="_blank">YouTube</a></li>
			</ul>
		</div>
	</header>
	<span class="header-overlay"></span>
</div>