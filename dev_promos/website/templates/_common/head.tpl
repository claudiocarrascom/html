<!DOCTYPE html>
<html lang="es">
<head>
	<title>{$site.title}</title>
	<meta name="description" content="{$site.description}" />
	<meta name="keywords" content="{$site.keywords}" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0">
	<meta http-equiv="content-language" content="es-cl">
	<meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="{$site.urlStatic}_common/images/favicon.ico">
	<link rel="apple-touch-icon" sizes="180x180" href="{$site.urlStatic}_common/images/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="{$site.urlStatic}_common/images/android-chrome-192x192.png" sizes="192x192">

	<meta name="expires" content="{$site.expires}">
	<meta name="author" content="mega.cl" />
	<meta name="organization" content="Red Televisiva Megavisión S.A." />
	<meta name="locality" content="Santiago, Chile" />
	<meta name="Robots" content="all" />

	{if strpos($smarty.server.REQUEST_URI, '/home/')!==false}
		<meta http-equiv="refresh" content="300">
	{/if}

	{if strpos($smarty.server.REQUEST_URI, '.html')!==false}
		<!-- FACEBOOK -->
		<meta property="og:type" content="article" />
		<meta property="og:title" content="{$data.articulo.titulo|replace:"\"":"&quot;"}" />
		<meta property="og:site_name" content="www.mega.cl"/>
		<meta property="og:url" content="{$data.articulo.url}" />
		<meta property="og:description" content="{$data.articulo.bajada|replace:"\"":"&quot;"}" />
		<meta property="og:image" content="{$data.articulo.imagen}?d=300x200" />
		<meta property="og:image:width" content="300" />
		<meta property="og:image:height" content="200" />
		<meta property="og:locale" content="es_CL" />
		<meta property="fb:app_id" content="200351320092613" />
	{/if}
	
	{if $site.isComputer}
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
	{/if}
	
	<!--[if gte IE 9]><!-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<!--<![endif]-->
	<!--[if lte IE 8]>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->	
	
	<link href="{$site.urlStatic}_common/css/main.css?d=30122015" rel="stylesheet" type="text/css" />
	<script src="{$site.urlStatic}_common/js/functions.js"></script>

	{if $site.head}
		{include file=$site.head}
	{/if}

	<script src="{$site.urlResources}js/common/common.js"></script>	
	
	{include file="./analytics.tpl"}
	
</head>