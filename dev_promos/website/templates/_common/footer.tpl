
	<!--FOOTER-->
	<footer class="hero-mega-footer">
		<div class="mega-main-width footer-col-wap clear">
			<div class="footer-col first-col">
				<h2>Programas</h2>
				<ul class="program-list">

				{foreach $data.programas as $row}
					<li><a href="{$row.url}">{$row.titulo}</a></li>
				{/foreach}

				</ul>
			</div>
			<div class="footer-col">
				<h2>Nuestra Red</h2>
				<ul class="program-list">
					<li><a href="http://www.mega.cl/" target="_blank">Mega</a></li>
					<li><a href="http://www.ahoranoticias.cl/" target="_blank">Ahora Noticias</a></li>
					<li><a href="http://www.estilomujer.cl/" target="_blank">Estilo Mujer</a></li>
					<li><a href="http://www.radiocandela.cl/" target="_blank">Radio Candela</a></li>
					<li><a href="http://www.etc.cl/" target="_blank">Etc</a></li>
				</ul>
			</div>
			<div class="footer-col">
				<h2>Corporativo</h2>
				<ul class="program-list">
					<li><a href="{$data.quienesSomos.rows.0.url}">Nuestra Marca</a></li>
					<li><a href="http://comercial.mega.cl/" target="_blank">Área Comercial</a></li>
					<li><a href="{$site.urlStatic}_common/docs/info_emision.pdf" target="_blank">Información de Emisión</a></li>
					<li><a href="{$site.urlStatic}_common/docs/info_emision_2014.pdf" target="_blank">Información de Emisión 2014</a></li>
					<li><a href="http://concursos.mega.cl/concursos/" target="_blank">Bases y ganadores concursos</a></li>
					<li><a href="{$site.urlStatic}_common/docs/orientaciones_4-feb-2015.pdf" target="_blank">Orientaciones Programáticas</a></li>
					<li><a href="http://www.bethia.cl/" target="_blank">Holding Bethia</a></li>
				</ul>
			</div>
			<div class="footer-col">
				<h2>Redes Sociales</h2>
				<ul class="social-wrap">
					<li class="social-item"><a href="{$data.facebook}" class="social-link fb" target="_blank">Facebook</a></li>
					<li class="social-item"><a href="{$data.twitter}" class="social-link tw" target="_blank">Twitter</a></li>
					<li class="social-item"><a href="{$data.instagram}" class="social-link ig" target="_blank">Instagram</a></li>
					<li class="social-item"><a href="{$data.youtube}" class="social-link yt" target="_blank">YouTube</a></li>
				</ul>
				<a class="button-normal work-with-us" href="http://mega.trabajando.cl/" target="_blank">Únete a nuestro Equipo</a>
			</div>
		</div>
		<div class="footer-end">
			<div class="mega-main-width">
				<a href="{$site.urlHome}"><img src="{$site.urlStatic}_common/images/logos/mega-white-small.png" class="mega-logo" width="43" height="43"></a>
				<p>Avenida Vicuña Mackenna #1370, Ñuñoa, Santiago Chile | Teléfono: 56-2-2810 8000</p>
			</div>
		</div>
	</footer>
