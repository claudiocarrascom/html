
<!-- Google Analytics -->
<script>
	{literal}
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	{/literal}
	ga('create', '{$site.analytics.id}', '{$site.analytics.site}');
	{if isset($data.enVivo) && $data.enVivo.urlVirtual}
		ga('set', 'page', '/senal-en-vivo/{$data.enVivo.urlVirtual}/');
		ga('set', 'title', 'SEÑAL EN VIVO - {$data.enVivo.titulo|upper}');
	{/if}

	ga('require', 'displayfeatures');
	ga('send', 'pageview');

	{literal}
	ga('create', 'UA-65891185-1', 'auto', {'name':'global'});
	ga('global.send', 'pageview');
	{/literal}
</script>
<!-- End Google Analytics -->

<!-- Begin comScore Tag -->
<script>
	var _comscore = _comscore || [];
	_comscore.push({ c1: "2", c2: "6906467" });
	(function() {
		var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
		el.parentNode.insertBefore(s, el);
	})();
</script>
<noscript>
	<img src="http://b.scorecardresearch.com/p?c1=2&c2=6906467&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->


