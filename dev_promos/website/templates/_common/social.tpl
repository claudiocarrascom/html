<section class="article-section sharer-section">
	<header class="medium-header"><h3>Compártelo en</h3></header>
	<ul class="social-wrap">
		<li class="social-item"><a href="http://www.facebook.com/share.php?u={$data.articulo.url}&title={$data.articulo.titulo}" class="social-link share-button fb" target="_blank">Facebook</a></li>
		<li class="social-item"><a href="http://twitter.com/intent/tweet?status={$data.articulo.titulo}+{$data.articulo.urlcorta}" class="social-link share-button tw" target="_blank">Twitter</a></li>
		{if !$site.isComputer}
		<li class="social-item"><a href="whatsapp://send?text={$data.articulo.titulo} {$data.articulo.url}" data-action="share/whatsapp/share" class="social-link share-button wap" target="_blank"><span>WhatsApp</span></a></li>
		{/if}
	</ul>
</section>