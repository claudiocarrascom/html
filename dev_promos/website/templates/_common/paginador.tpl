﻿{if isset($data.articulos.pages) && $data.articulos.pages > 1}
{1}
	<div class="paginador-wrap">
		<div class="paginador-container">
	
			{if $data.articulos.page > 1 + 4}
				<a href="{$site.urlBase}{$data.paginador}page=1" class="p-item"> << </a>
				<a href="{$site.urlBase}{$data.paginador}page={$data.articulos.page-1}" class="p-item"> < </a>
			{/if}

			{for $page = $data.articulos.page-4 to $data.articulos.page+4}
				{if $page >= 1 and $page <= $data.articulos.pages}
					{if $page==$data.articulos.page}
						<span class="p-item current">{$page}</span>
					{else}
						<a href="{$site.urlBase}{$data.paginador}page={$page}" class="p-item">{$page}</a>
					{/if}
				{/if}
			{/for}

			{if $data.articulos.page < $data.articulos.pages-4}
				<a href="{$site.urlBase}{$data.paginador}page={$data.articulos.page+1}" class="p-item"> > </a>
				<a href="{$site.urlBase}{$data.paginador}page={$data.articulos.pages}" class="p-item"> >> </a>
			{/if}
		</div>
	</div>
{/if}