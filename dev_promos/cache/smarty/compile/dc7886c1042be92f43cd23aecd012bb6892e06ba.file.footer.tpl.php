<?php /* Smarty version Smarty-3.1.19, created on 2017-08-11 23:45:20
         compiled from "C:\AppServ\www\html\dev_promos\website\templates\_common\footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10276598e79d0459e42-80113910%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dc7886c1042be92f43cd23aecd012bb6892e06ba' => 
    array (
      0 => 'C:\\AppServ\\www\\html\\dev_promos\\website\\templates\\_common\\footer.tpl',
      1 => 1453145982,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10276598e79d0459e42-80113910',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'data' => 0,
    'row' => 0,
    'site' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_598e79d04b9423_05847590',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_598e79d04b9423_05847590')) {function content_598e79d04b9423_05847590($_smarty_tpl) {?>
	<!--FOOTER-->
	<footer class="hero-mega-footer">
		<div class="mega-main-width footer-col-wap clear">
			<div class="footer-col first-col">
				<h2>Programas</h2>
				<ul class="program-list">

				<?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value['programas']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
?>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['row']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['titulo'];?>
</a></li>
				<?php } ?>

				</ul>
			</div>
			<div class="footer-col">
				<h2>Nuestra Red</h2>
				<ul class="program-list">
					<li><a href="http://www.mega.cl/" target="_blank">Mega</a></li>
					<li><a href="http://www.ahoranoticias.cl/" target="_blank">Ahora Noticias</a></li>
					<li><a href="http://www.estilomujer.cl/" target="_blank">Estilo Mujer</a></li>
					<li><a href="http://www.radiocandela.cl/" target="_blank">Radio Candela</a></li>
					<li><a href="http://www.etc.cl/" target="_blank">Etc</a></li>
				</ul>
			</div>
			<div class="footer-col">
				<h2>Corporativo</h2>
				<ul class="program-list">
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['quienesSomos']['rows'][0]['url'];?>
">Nuestra Marca</a></li>
					<li><a href="http://comercial.mega.cl/" target="_blank">Área Comercial</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/docs/info_emision.pdf" target="_blank">Información de Emisión</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/docs/info_emision_2014.pdf" target="_blank">Información de Emisión 2014</a></li>
					<li><a href="http://concursos.mega.cl/concursos/" target="_blank">Bases y ganadores concursos</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/docs/orientaciones_4-feb-2015.pdf" target="_blank">Orientaciones Programáticas</a></li>
					<li><a href="http://www.bethia.cl/" target="_blank">Holding Bethia</a></li>
				</ul>
			</div>
			<div class="footer-col">
				<h2>Redes Sociales</h2>
				<ul class="social-wrap">
					<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['facebook'];?>
" class="social-link fb" target="_blank">Facebook</a></li>
					<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['twitter'];?>
" class="social-link tw" target="_blank">Twitter</a></li>
					<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['instagram'];?>
" class="social-link ig" target="_blank">Instagram</a></li>
					<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['youtube'];?>
" class="social-link yt" target="_blank">YouTube</a></li>
				</ul>
				<a class="button-normal work-with-us" href="http://mega.trabajando.cl/" target="_blank">Únete a nuestro Equipo</a>
			</div>
		</div>
		<div class="footer-end">
			<div class="mega-main-width">
				<a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlHome'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/images/logos/mega-white-small.png" class="mega-logo" width="43" height="43"></a>
				<p>Avenida Vicuña Mackenna #1370, Ñuñoa, Santiago Chile | Teléfono: 56-2-2810 8000</p>
			</div>
		</div>
	</footer>
<?php }} ?>
