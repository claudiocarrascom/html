<?php /* Smarty version Smarty-3.1.19, created on 2017-08-11 23:45:20
         compiled from "C:\AppServ\www\html\dev_promos\website\templates\_common\header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5046598e79d0256e80-05985344%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6e41a2e7e72740f083f5e7c4bffb8c2139b0f4a6' => 
    array (
      0 => 'C:\\AppServ\\www\\html\\dev_promos\\website\\templates\\_common\\header.tpl',
      1 => 1459375102,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5046598e79d0256e80-05985344',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'site' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_598e79d030a328_50962646',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_598e79d030a328_50962646')) {function content_598e79d030a328_50962646($_smarty_tpl) {?><div class="mega-main-width mega-main-head-wrap">
	<header class="mega-main-head">
		<?php if (!($_smarty_tpl->tpl_vars['site']->value['isSmartphone'])) {?>
			<a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
" class="go-home"><img src="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/images/logos/logo-mega-animado.gif" class="mega-logo" width="90" height="116"></a>
		<?php }?>
		<div class="menu-toggle vert-center">
			<img src="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/images/logos/mega-white-small.png" class="mega-logo" width="80" height="83">
			<div class="mega-toggle-nav">
				<span class="bar-top"></span>
				<span class="bar-mid"></span>
				<span class="bar-bot"></span>
				<p class="menu-text vert-center"></p>
			</div>
		</div>
		<div class="mobile-lb-menu nav-wrap vert-center">
			<nav class="center-content">
				<ul class="text-buttons">
					<li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
" class="nav-link">Home</a></li>
					<li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
exclusivo/" class="nav-link">Imperdibles</a></li>
					<li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
programas/" class="nav-link">Programas</a></li>
					<li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
teleseries/" class="nav-link">Teleseries</a></li>
					<li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
lo-mas-de-youtube/" class="nav-link">Lo <i>+</i> de YouTube</a></li>
					<li class="nav-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
mega-eventos/" class="nav-link">Eventos</a></li>
					<li class="nav-item"><a href="http://tramites.ahoranoticias.cl/" target="_blank" class="nav-link">Mega Trámites</a></li>
					<li class="nav-item"><a href="http://www.ahoranoticias.cl" target="_blank" class="nav-link">Noticias</a></li>
				</ul>

				<ul class="capsulas">
					<li class="nav-item live-item m25-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
25/" target="_blank" class="nav-link">25 Años Mega</a></li>
					<li class="nav-item live-item la-roja-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
la-roja/" target="_blank" class="nav-link"><span>Sitio oficial de</span> la Roja</a></li>
					<li class="nav-item live-item cap-completos"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
capitulos-completos/teleseries/" class="nav-link">Capítulos Completos</a></li>
					<li class="nav-item live-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
espectaculos/" class="nav-link">Espectáculos</a></li>
					<li class="nav-item live-item espect-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
causas/" class="nav-link">Mi Causa mi MEGA</a></li>
					<li class="nav-item live-item"><a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
senal-en-vivo/" class="nav-link"><span>Señal</span> en Vivo</a></li>
				</ul>

				<ul class="social-wrap">
					<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['facebook'];?>
" class="social-link fb" target="_blank">Facebook</a></li>
					<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['twitter'];?>
" class="social-link tw" target="_blank">Twitter</a></li>
					<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['instagram'];?>
" class="social-link ig" target="_blank">Instagram</a></li>
					<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['youtube'];?>
" class="social-link yt" target="_blank">YouTube</a></li>
				</ul>
			</nav>
		</div>
		<!--en vivo mobile-->
		<div class="vert-center show-on-mobile-nav">
			<a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
senal-en-vivo/" class="mobile-nav-link">En Vivo</a>
		</div>
		<!--buscador boton-->
		<div class="vert-center nav-right">
			<span class="circle-button search-icon toggle-search"></span>
			<a href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
buscador/" class="circle-button search-icon mobile-search-button"></a>
		</div>
		<!--buscador-->
		<div class="toggle-search-wrap vert-center">
			<form method="get" action="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlBase'];?>
buscador/" class="buscador-home">
				<input type="text" placeholder="Buscar" value="" name="q" class="input-home">
				<input type="submit" onclick="this.form.submit();return false;" value="" name="buscar" class="search-button-home">
			</form>
		</div>
		<!--redes sociales-->
		<div class="vert-center nav-right social-toggler">
			<span class="circle-button social-icon">Redes Sociales</span>
			<ul class="social-wrap toc-socials">
				<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['facebook'];?>
" class="social-link fb" target="_blank">Facebook</a></li>
				<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['twitter'];?>
" class="social-link tw" target="_blank">Twitter</a></li>
				<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['instagram'];?>
" class="social-link ig" target="_blank">Instagram</a></li>
				<li class="social-item"><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['youtube'];?>
" class="social-link yt" target="_blank">YouTube</a></li>
			</ul>
		</div>
	</header>
	<span class="header-overlay"></span>
</div><?php }} ?>
