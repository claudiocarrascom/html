<?php /* Smarty version Smarty-3.1.19, created on 2017-08-11 23:45:20
         compiled from "C:\AppServ\www\html\dev_promos\website\templates\_common\head.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15480598e79d005b8d5-45293671%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0123b4beb7b01ad6ff73f237a5ee6d83006d2381' => 
    array (
      0 => 'C:\\AppServ\\www\\html\\dev_promos\\website\\templates\\_common\\head.tpl',
      1 => 1502465304,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15480598e79d005b8d5-45293671',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'site' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_598e79d014be00_53696115',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_598e79d014be00_53696115')) {function content_598e79d014be00_53696115($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include 'C:\\AppServ\\www\\html\\_core\\plugins\\Smarty-3.1.19\\libs\\plugins\\modifier.replace.php';
?><!DOCTYPE html>
<html lang="es">
<head>
	<title><?php echo $_smarty_tpl->tpl_vars['site']->value['title'];?>
</title>
	<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['site']->value['description'];?>
" />
	<meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['site']->value['keywords'];?>
" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0">
	<meta http-equiv="content-language" content="es-cl">
	<meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/images/favicon.ico">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/images/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/images/android-chrome-192x192.png" sizes="192x192">

	<meta name="expires" content="<?php echo $_smarty_tpl->tpl_vars['site']->value['expires'];?>
">
	<meta name="author" content="mega.cl" />
	<meta name="organization" content="Red Televisiva Megavisión S.A." />
	<meta name="locality" content="Santiago, Chile" />
	<meta name="Robots" content="all" />

	<?php if (strpos($_SERVER['REQUEST_URI'],'/home/')!==false) {?>
		<meta http-equiv="refresh" content="300">
	<?php }?>

	<?php if (strpos($_SERVER['REQUEST_URI'],'.html')!==false) {?>
		<!-- FACEBOOK -->
		<meta property="og:type" content="article" />
		<meta property="og:title" content="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['data']->value['articulo']['titulo'],"\"","&quot;");?>
" />
		<meta property="og:site_name" content="www.mega.cl"/>
		<meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['data']->value['articulo']['url'];?>
" />
		<meta property="og:description" content="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['data']->value['articulo']['bajada'],"\"","&quot;");?>
" />
		<meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['data']->value['articulo']['imagen'];?>
?d=300x200" />
		<meta property="og:image:width" content="300" />
		<meta property="og:image:height" content="200" />
		<meta property="og:locale" content="es_CL" />
		<meta property="fb:app_id" content="200351320092613" />
	<?php }?>
	
	<?php if ($_smarty_tpl->tpl_vars['site']->value['isComputer']) {?>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
	<?php }?>
	
	<!--[if gte IE 9]><!-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<!--<![endif]-->
	<!--[if lte IE 8]>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->	
	
	<link href="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/css/main.css?d=30122015" rel="stylesheet" type="text/css" />
	<script src="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlStatic'];?>
_common/js/functions.js"></script>

	<?php if ($_smarty_tpl->tpl_vars['site']->value['head']) {?>
		<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['site']->value['head'], $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	<?php }?>

	<script src="<?php echo $_smarty_tpl->tpl_vars['site']->value['urlResources'];?>
js/common/common.js"></script>	
	
	<?php echo $_smarty_tpl->getSubTemplate ("./analytics.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	
</head><?php }} ?>
