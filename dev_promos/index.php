<?php

require_once 'includes/common/Config.class.php';

require_once Config::create()->getCoreBussiness().'Includes.class.php';

/**
 * Clase controladora principal
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class MainController {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		Includes::create()->showErrors();
	}

	/**
	 * Inicio del despliegue
	 */
	public function main()
	{
		if ( Url::create()->check() ) {
			
			$urlVo = Url::create()->getUrlVO();
			
			$class = $urlVo->getClaseVO()->getnombre();

			$method = $urlVo->getTemplateVO()->getnombre();

			if (Includes::create()->checkIncludes(Config::create()->getWebsiteBussiness() . $class . '.class.php')) {

				try {
					if (!class_exists($class))
						throw new Exception('No existe Clase: ' . $class);

					/**
					 * @var Module $class
					 */
					$class = new $class($urlVo);

					if (!method_exists($class, $method))
						throw new Exception('No existe Método: "'.$method.'" en Clase: '. get_class($class));

					$class->$method();
				}
				catch (Exception $e) {
					FLogger::create()->error($e);
				}
			}
		}
	}
}

$index = new MainController();
$index->main();

?>