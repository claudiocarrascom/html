<?php

global $config;

$config = array();

/******************
 SISTEMA
******************/

$config['site']['title']		= 'Title';
$config['site']['description']	= 'Description';
$config['site']['keywords']		= 'Keywords';

$config['site']['facebook']		= 'https://www.facebook.com/MEGACL';
$config['site']['twitter']		= 'https://twitter.com/mega';
$config['site']['youtube']		= 'https://www.youtube.com/user/programasmega?sub_confirmation=1';
$config['site']['instagram']	= 'https://instagram.com/MEGA.TV/';


/******************
 DATABASE
******************/

$config['database']['host']		= 'localhost';

$config['database']['db']		= 'pdp_dev';

$config['database']['user']		= 'root';

$config['database']['pass']		= 'iluminado';


/******************
 DIRECTORIOS
******************/

$config['dir']['app']			= 'website';

$config['dir']['cache']			= 'cache';

$config['dir']['logs']			= 'logs';


/******************
 RECURSOS EXTERNOS
******************/

$config['http']['images']		= 'http://localhost/html/images';

$config['http']['resources']	= 'http://localhost/html/servicios/resources';

$config['http']['static']		= 'http://localhost/html/dev_promos/website/templates/_static'; 

$config['http']['videos'][1]	= 'http://mdstrm.com/embed/';


/******************
 METRICAS
******************/

$config['analytics']['id']		= 'UA-17215798-1';
$config['analytics']['site']	= 'auto';

$config['comscore']['id']		= '6906467';
$config['comscore']['site']		= 'mega-cl';


/******************
 CACHE (minutos)
******************/

$config['cache']['adodb']		= 1;

$config['cache']['smarty']		= 0;

?>