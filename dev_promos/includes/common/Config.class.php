<?php

/**
 * Clase de configuración general
 * @author:		Claudio Carrasco M.
 * @date:		10-Ago-2017
 * @version:	1.0.0
 */
class Config {

	private static $singleton;

	/**
	 * Variable de configuración de BBDD
	 */
	private $db;

	/**
	 * Variables de servidor
	 */
	private $serverRoot;
	private $serverLog;
	private $serverCache;
	private $serverCore;
	private $serverWebsite;

	/**
	 * Variables HTTP
	 */
	private $httpRoot;
	private $httpUri;
	private $httpBase;
	private $httpWebsite;
	private $httpImages;
	private $httpStatic;
	private $httpVideos;
	private $httpResources;

	/**
	 * Otras Variables
	 */
	private $logFile;
	private $logFileAdBlock;
	private $cacheAdodb;
	private $cacheSmarty;

	private $analyticsId;
	private $analyticsSite;
	private $comscoreId;
	private $comscoreSite;

	private $siteTitle;
	private $siteDescription;
	private $siteKeywords;

	private $facebook;
	private $twitter;
	private $youtube;
	private $instagram;

	/**
	 * Constructor
	 */
	private function __construct()
	{
		$this->serverRoot			= dirname(__FILE__);
		
		$this->serverRoot			= substr($this->serverRoot, 0, strpos($this->serverRoot, '/includes')).'/';
		$this->serverRoot			= dirname($_SERVER['SCRIPT_FILENAME']).'/';

		
		require_once($this->serverRoot.'includes/config.inc');
		global $config;

		/**
		 * Variables de Servidor
		 */
		$this->serverCore			= realpath($this->serverRoot . '../_core/').'/';

		$this->serverWebsite		= $this->serverRoot.$config['dir']['app'].'/';

		$this->serverLog			= $this->serverRoot.$config['dir']['logs'].'/';

		$this->serverCache			= $this->serverRoot.$config['dir']['cache'].'/';

		/**
		 * Variables HTTP
		 */
		$this->httpRoot				= 'http://'.$_SERVER['HTTP_HOST'];

		$this->httpUri				= dirname($_SERVER['PHP_SELF']);

		$this->httpUri				= $this->httpUri . (substr($this->httpUri, -1, 1)=='/' ? '' : '/');

		$this->httpBase				= $this->httpRoot.$this->httpUri;

		$this->httpWebsite			= $this->httpBase.$config['dir']['app'].'/templates/';

		$this->httpImages			= $config['http']['images'].'/';

		$this->httpStatic			= $config['http']['static'].'/';

		$this->httpVideos			= $config['http']['videos'].'/';

		$this->httpResources		= $config['http']['resources'].'/';

		/**
		 * Archivo Log
		 */
		$this->logFile				= 'mega';		


		/**
		 * Tiempos de Caché
		 */
		$this->cacheAdodb			= $config['cache']['adodb'];

		$this->cacheSmarty			= $config['cache']['smarty'];

		/**
		 * Variables de Analítica
		 */
		$this->analyticsId			= $config['analytics']['id'];
		$this->analyticsSite		= $config['analytics']['site'];

		$this->comscoreId			= $config['comscore']['id'];
		$this->comscoreSite			= $config['comscore']['site'];

		/**
		 * Otras variables
		 */
		$this->siteTitle			= $config['site']['title'];
		$this->siteDescription		= $config['site']['description'];
		$this->siteKeywords			= $config['site']['keywords'];

		$this->facebook				= $config['site']['facebook'];
		$this->twitter				= $config['site']['twitter'];
		$this->youtube				= $config['site']['youtube'];
		$this->instagram			= $config['site']['instagram'];
	}

	/**
	 * Obtiene instancia de clase.
	 * @return Config Instancia singleton
	 */
	public static function create()
	{
		if ( is_null(self::$singleton) )
			self::$singleton = new self;
		return self::$singleton;
	}

	/**
	 * Base Datos principal o por defecto
	 */
	public function setDataBase()
	{
		require_once($this->serverRoot.'includes/config.inc');
		global $config;

		$this->db = new stdClass();

		$this->db->host		= $config['database']['host'];

		$this->db->dataBase	= $config['database']['db'];

		$this->db->user		= $config['database']['user'];

		$this->db->pass		= $config['database']['pass'];

		$this->db->driver	= 'mysqli';
	}

	/**
	 * Retorna el HTTP_HOST
	 * @return string
	 */
	public function getHttpRoot()
	{
		return $this->httpRoot;
	}

	/**
	 * Retorna el HTTP_HOST
	 * @return string
	 */
	public function getHttpUri()
	{
		return $this->httpUri;
	}

	/**
	 * Retorna el HTTP_HOST + la carpeta donde se encuentra el sitio
	 * @return string
	 */
	public function getHttpBase()
	{
		return $this->httpBase;
	}

	/**
	 * Retorna la url al Home del sitio
	 * @return string
	 */
	public function getHttpHome()
	{
		return $this->httpBase.'home/';
	}

	/**
	 * Retorna la url al Home del sitio
	 * @return string
	 */
	public function getHttpWebsite()
	{
		return $this->httpWebsite;
	}

	/**
	 * Retorna la url al repositorio de imágenes
	 * @return string
	 */
	public function getHttpImages()
	{
		return $this->httpImages;
	}

	/**
	 * Retorna la url al repositorio de archivos estáticos. Ej: CSS, JS, Imágenes del sitio
	 * @return string
	 */
	public function getHttpStatic()
	{
		return $this->httpStatic;
	}

	/**
	 * Retorna la url al repositorio de videos
	 * @return string
	 */
	public function getHttpVideos()
	{
		return $this->httpVideos;
	}

	/**
	 * Retorna la url al repositorio de Recursos
	 * @return string
	 */
	public function getHttpResources()
	{
		return $this->httpResources;
	}

	/**
	 * Retorna la ruta a la raíz del sitio
	 * @return string
	 */
	public function getServerRoot()
	{
		return $this->serverRoot;
	}

	/**
	 * Retorna la ruta a la carpeta de los Logs
	 * @return string
	 */
	public function getServerLog()
	{
		return $this->serverLog;
	}
	
	/**
	 * Retorna la ruta a la carpeta de Caché
	 * @return string
	 */
	public function getServerCache()
	{
		return $this->serverCache;
	}

	/**
	 * Retorna la ruta a la carpeta de Imagenes en Server
	 * @return string
	 */
	public function getServerImages()
	{
		return '';
	}

	/**
	 * Retorna la ruta a la carpeta Website
	 * @return string
	 */
	public function getServerWebsite()
	{
		return $this->serverWebsite;
	}

	/**
	 * Retorna la ruta a la carpeta Bussiness
	 * @return string
	 */
	public function getCoreBussiness()
	{
		return $this->serverCore.'class/bussiness/';
	}

	/**
	 * Retorna la ruta a la carpeta Data
	 * @return string
	 */
	public function getCoreData()
	{
		return $this->serverCore.'class/data/';
	}

	/**
	 * Retorna la ruta a la carpeta Model
	 * @return string
	 */
	public function getCoreModel()
	{
		return $this->serverCore.'class/model/';
	}

	/**
	 * Retorna la ruta a la carpeta de Plugins
	 * @return string
	 */
	public function getCorePlugins()
	{
		return $this->serverCore.'plugins/';
	}

	/**
	 * Retorna la ruta a la carpeta Bussiness
	 * @return string
	 */
	public function getWebsiteBussiness()
	{
		return $this->serverWebsite.'bussiness/';
	}

	/**
	 * Retorna la ruta a la carpeta Data
	 * @return string
	 */
	public function getWebsiteData()
	{
		return $this->serverWebsite.'data/';
	}

	/**
	 * Retorna la ruta a la carpeta Model
	 * @return string
	 */
	public function getWebsiteModel()
	{
		return $this->serverWebsite.'model/';
	}

	/**
	 * Retorna la ruta a la carpeta Views
	 * @return string
	 */
	public function getWebsiteTemplates()
	{
		return $this->serverWebsite.'templates/';
	}

	/**
	 * Métodos de BBDD
	 */
	public function getDatabase()
	{
		return $this->db;
	}

	/**
	 * Devuelve nombre del archivo Log
	 * @return string
	 */
	public function getLogFile()
	{
		return $this->logFile;
	}

	/**
	 * Retorna el tiempo en minutos de cache de los querys con Adodb
	 * @return int
	 */
	public function getAdodbCacheTime()
	{
		return $this->cacheAdodb;
	}

	/**
	 * Retorna el tiempo en minutos de cache de las plantillas de Smarty
	 * @return int
	 */
	public function getSmartyCacheTime()
	{
		return $this->cacheSmarty;
	}

	/**
	 * Retorna la ruta de Smarty Template
	 * @return string
	 */
	public function getSmartyTemplate()
	{
		return $this->getWebsiteTemplates();
	}

	/**
	 * Retorna la ruta de Smarty Compile
	 * @return string
	 */
	public function getSmartyCompile()
	{
		return $this->serverCache.'smarty/compile/';
	}

	/**
	 * Retorna la ruta de Smarty Config
	 * @return string
	 */
	public function getSmartyConfig()
	{
		return $this->serverCache.'smarty/config/';
	}

	/**
	 * Retorna la ruta de Smarty Cache
	 * @return string
	 */
	public function getSmartyCache()
	{
		return $this->serverCache.'smarty/cache/';
	}

	/**
	 * Retorna el ID de Google Analytics
	 * @return string
	 */
	public function getAnalyticsId()
	{
		return $this->analyticsId;
	}

	/**
	 * Retorna el Site de Google Analytics
	 * @return string
	 */
	public function getAnalyticsSite()
	{
		return $this->analyticsSite;
	}

	/**
	 * Retorna el ID de comScore
	 * @return string
	 */
	public function getComScoreId()
	{
		return $this->comscoreId;
	}

	/**
	 * Retorna el Site de comScore
	 * @return string
	 */
	public function getComScoreSite()
	{
		return $this->comscoreSite;
	}

	/**
	 * Retorna el título general del Sitio
	 * @return string
	 */
	public function getSiteTitle()
	{
		return $this->siteTitle;
	}

	/**
 * Retorna la descripción general del Sitio
 * @return string
 */
	public function getSiteDescription()
	{
		return $this->siteDescription;
	}

	/**
	 * Retorna las keywords generales del Sitio
	 * @return string
	 */
	public function getSiteKeywords()
	{
		return $this->siteKeywords;
	}

	/**
	 * Retorna la ruta del FanPage del sitio
	 * @return string
	 */
	public function getFacebookUrl()
	{
		return $this->facebook;
	}

	/**
	 * Retorna la ruta de la página en Twitter
	 * @return string
	 */
	public function getTwitterUrl()
	{
		return $this->twitter;
	}

	/**
	 * Retorna la ruta del Canal de Youtube
	 * @return string
	 */
	public function getYoutubeUrl()
	{
		return $this->youtube;
	}

	/**
	 * Retorna la ruta de la página en Instagram
	 * @return string
	 */
	public function getInstagramUrl()
	{
		return $this->instagram;
	}
}

?>